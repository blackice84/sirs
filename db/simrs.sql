/*
SQLyog Enterprise - MySQL GUI v8.05 
MySQL - 5.7.19 : Database - simrsdb
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

CREATE DATABASE /*!32312 IF NOT EXISTS*/`simrsdb` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `simrsdb`;

/*Table structure for table `tbl_diagnosa` */

DROP TABLE IF EXISTS `tbl_diagnosa`;

CREATE TABLE `tbl_diagnosa` (
  `diagnosa_id` int(11) NOT NULL AUTO_INCREMENT,
  `diagnosa_kode` varchar(10) DEFAULT NULL,
  `diagnosa_nama` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`diagnosa_id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;

/*Data for the table `tbl_diagnosa` */

insert  into `tbl_diagnosa`(`diagnosa_id`,`diagnosa_kode`,`diagnosa_nama`) values (1,'A00','Cholera'),(2,'A00.0','Cholera due to Vibrio cholerae 01, biovar cholerae'),(3,'A00.1','Cholera due to Vibrio cholerae 01, biovar el tor'),(4,'A00.9','Cholera, unspecified'),(5,'A01','Typhoid and paratyphoid fevers'),(6,'A01.0','Typhoid fever'),(7,'A01.1','Paratyphoid fever A'),(8,'A01.2','Paratyphoid fever B'),(9,'A01.3','Paratyphoid fever C'),(10,'A01.4','Paratyphoid fever, unspecified'),(11,'A02','Other salmonella infections'),(12,'A02.0','Salmonella gastroenteritis'),(13,'A02.1','Salmonella septicemia'),(14,'A02.2','Localized salmonella infections');

/*Table structure for table `tbl_jenislayanan` */

DROP TABLE IF EXISTS `tbl_jenislayanan`;

CREATE TABLE `tbl_jenislayanan` (
  `jenlan_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'jenis layanan',
  `jenlan_nama` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`jenlan_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

/*Data for the table `tbl_jenislayanan` */

insert  into `tbl_jenislayanan`(`jenlan_id`,`jenlan_nama`) values (1,'Rawat Jalan'),(2,'Rawat Inap'),(3,'Rawat Darurat'),(4,'Laboratorium'),(5,'Radiologi'),(6,'OK'),(7,'Hemodialisa');

/*Table structure for table `tbl_kabupaten` */

DROP TABLE IF EXISTS `tbl_kabupaten`;

CREATE TABLE `tbl_kabupaten` (
  `kabupaten_id` int(11) NOT NULL AUTO_INCREMENT,
  `kabupaten_nama` varchar(200) DEFAULT NULL,
  `kabupaten_prop_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`kabupaten_id`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=latin1;

/*Data for the table `tbl_kabupaten` */

insert  into `tbl_kabupaten`(`kabupaten_id`,`kabupaten_nama`,`kabupaten_prop_id`) values (1,'Aceh Barat ',1),(2,'Aceh Barat Daya',1),(3,'Aceh Besar',1),(4,'Aceh Jaya',1),(5,'Aceh Selatan',1),(6,'Aceh Singkil',1),(7,'Aceh Tamiang',1),(8,'Aceh Tengah',1),(9,'Aceh Tenggara',1),(10,'Aceh Timur',1),(11,'Aceh Utara',1),(12,'Banda Aceh',1),(13,'Bener Meriah',1),(14,'Bireuen',1),(15,'Gayo Lues',1),(16,'Lhokseumawe',1),(17,'Nagan Raya',1),(18,'Pidie',1),(19,'Pidie Jaya',1),(20,'Sabang',1),(21,'Simeulue',1),(22,'Subulussalam',1);

/*Table structure for table `tbl_kamar` */

DROP TABLE IF EXISTS `tbl_kamar`;

CREATE TABLE `tbl_kamar` (
  `kamar_id` int(11) NOT NULL AUTO_INCREMENT,
  `kamar_ruangan` varchar(45) DEFAULT NULL,
  `kamar_bed` char(2) DEFAULT NULL,
  `kamar_kelas_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`kamar_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

/*Data for the table `tbl_kamar` */

insert  into `tbl_kamar`(`kamar_id`,`kamar_ruangan`,`kamar_bed`,`kamar_kelas_id`) values (1,'Edelweis','1',1),(2,'Edelweis','2',1),(3,'Edelweis','3',1),(4,'Edelweis','4',1);

/*Table structure for table `tbl_kecamatan` */

DROP TABLE IF EXISTS `tbl_kecamatan`;

CREATE TABLE `tbl_kecamatan` (
  `kecamatan_id` int(11) NOT NULL AUTO_INCREMENT,
  `kecamatan_nama` varchar(200) DEFAULT NULL,
  `kecamatan_kab_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`kecamatan_id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;

/*Data for the table `tbl_kecamatan` */

insert  into `tbl_kecamatan`(`kecamatan_id`,`kecamatan_nama`,`kecamatan_kab_id`) values (1,'Arogan Lambalek',1),(10,'Bubon',1),(14,'Johan Pahlawan',1);

/*Table structure for table `tbl_kelas` */

DROP TABLE IF EXISTS `tbl_kelas`;

CREATE TABLE `tbl_kelas` (
  `kelas_id` int(11) NOT NULL AUTO_INCREMENT,
  `kelas_nama` char(5) DEFAULT NULL,
  PRIMARY KEY (`kelas_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

/*Data for the table `tbl_kelas` */

insert  into `tbl_kelas`(`kelas_id`,`kelas_nama`) values (1,'1'),(2,'2'),(3,'3'),(4,'VIP'),(5,'VVIP');

/*Table structure for table `tbl_kelurahan` */

DROP TABLE IF EXISTS `tbl_kelurahan`;

CREATE TABLE `tbl_kelurahan` (
  `kelurahan_id` int(11) NOT NULL AUTO_INCREMENT,
  `kelurahan_nama` varchar(200) DEFAULT NULL,
  `kelurahan_kec_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`kelurahan_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Data for the table `tbl_kelurahan` */

insert  into `tbl_kelurahan`(`kelurahan_id`,`kelurahan_nama`,`kelurahan_kec_id`) values (1,'Alue Bagok',1);

/*Table structure for table `tbl_layanan` */

DROP TABLE IF EXISTS `tbl_layanan`;

CREATE TABLE `tbl_layanan` (
  `layanan_id` int(11) NOT NULL AUTO_INCREMENT,
  `layanan_tgl_masuk` date DEFAULT NULL,
  `layanan_tgl_keluar` date DEFAULT NULL,
  `layanan_cara_keluar` varchar(50) DEFAULT NULL,
  `layanan_sts_krs` varchar(20) DEFAULT NULL,
  `layanan_status` int(11) DEFAULT NULL,
  `lay_jenlan_id` int(11) DEFAULT NULL,
  `lay_unit_id` int(11) DEFAULT NULL,
  `lay_pasien_id` int(11) DEFAULT NULL,
  `lay_diagnosa_id` int(11) DEFAULT NULL,
  `lay_kelas_id` int(11) DEFAULT NULL,
  `lay_kamar_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`layanan_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `tbl_layanan` */

/*Table structure for table `tbl_pasien` */

DROP TABLE IF EXISTS `tbl_pasien`;

CREATE TABLE `tbl_pasien` (
  `pasien_id` int(11) NOT NULL AUTO_INCREMENT,
  `pasien_norm` char(8) DEFAULT NULL,
  `pasien_nik` varchar(50) DEFAULT NULL,
  `pasien_nama` varchar(100) DEFAULT NULL,
  `pasien_jenkel` char(1) DEFAULT NULL,
  `pasien_tlahir` varchar(200) DEFAULT NULL,
  `pasien_dlahir` date DEFAULT NULL,
  `pasien_agama` varchar(45) DEFAULT NULL,
  `pasien_prop_id` int(11) DEFAULT NULL,
  `pasien_kabupaten_id` int(11) DEFAULT NULL,
  `pasien_kecamatan_id` int(11) DEFAULT NULL,
  `pasien_kelurahan_id` int(11) DEFAULT NULL,
  `pasien_alamat` varchar(200) DEFAULT NULL,
  `pasien_telp` varchar(20) DEFAULT NULL,
  `pasien_pekerjaan` varchar(100) DEFAULT NULL,
  `pasien_pendidikan` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`pasien_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `tbl_pasien` */

/*Table structure for table `tbl_pegawai` */

DROP TABLE IF EXISTS `tbl_pegawai`;

CREATE TABLE `tbl_pegawai` (
  `pegawai_id` int(11) NOT NULL AUTO_INCREMENT,
  `pegawai_nama` varchar(200) DEFAULT NULL,
  `pegawai_alamat` varchar(200) DEFAULT NULL,
  `pegawai_telp` varchar(25) DEFAULT NULL,
  `pegawai_user` varchar(100) DEFAULT NULL,
  `pegawai_pass` varchar(50) DEFAULT NULL,
  `pegawai_level` int(11) DEFAULT NULL,
  `pegawai_jabatan` varchar(50) DEFAULT NULL,
  `pegawai_unit_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`pegawai_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

/*Data for the table `tbl_pegawai` */

insert  into `tbl_pegawai`(`pegawai_id`,`pegawai_nama`,`pegawai_alamat`,`pegawai_telp`,`pegawai_user`,`pegawai_pass`,`pegawai_level`,`pegawai_jabatan`,`pegawai_unit_id`) values (1,'Miftah Faridl','Rungkut Menanggal Harapan V10','082139231121','miftah','21232f297a57a5a743894a0e4a801fc3',2,'Staff IT',1),(2,'Administrator','Jalan A. Yani','0821','admin','21232f297a57a5a743894a0e4a801fc3',1,'Admin IT',1),(4,'Etik','Jalan Kemangi 21 Sidoarjo','081','etik','12345',1,'Perawat Poli Obgyn',10);

/*Table structure for table `tbl_propinsi` */

DROP TABLE IF EXISTS `tbl_propinsi`;

CREATE TABLE `tbl_propinsi` (
  `prop_id` int(11) NOT NULL AUTO_INCREMENT,
  `prop_nama` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`prop_id`)
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=latin1;

/*Data for the table `tbl_propinsi` */

insert  into `tbl_propinsi`(`prop_id`,`prop_nama`) values (1,'Nangroe Aceh Darussalam'),(2,'Sumatera Utara'),(3,'Sumatera Barat'),(4,'Kepulauan Riau'),(5,'Riau'),(6,'Jambi'),(7,'Sumatera Selatan'),(8,'Banka Belitung'),(9,'Lampung'),(10,'Bengkulu'),(11,'DKI Jakarta'),(12,'Jawa Barat'),(13,'Jawa Tengah'),(14,'D.I.Yogyakarta'),(15,'Banten'),(16,'Bali '),(17,'Nusa Tenggara Barat'),(18,'Nusa Tenggara Timur'),(19,'Kalimantan Utara'),(20,'Kalimantan Tengah'),(21,'Kalimantan Timur'),(22,'Kalimantan Selatan'),(23,'Kalimantan Barat'),(24,'Sulawesi Utara'),(25,'Sulawesi Tengah'),(26,'Sulawesi Barat'),(27,'Sulawesi Tenggara'),(28,'Sulawesi Selatan'),(29,'Gorontalo'),(30,'Maluku Utara'),(31,'Maluku'),(32,'Papua'),(33,'Papua Barat'),(34,'Teluk Cendrawasih');

/*Table structure for table `tbl_tindakan` */

DROP TABLE IF EXISTS `tbl_tindakan`;

CREATE TABLE `tbl_tindakan` (
  `tindakan_id` int(11) NOT NULL AUTO_INCREMENT,
  `tindakan_kode` char(5) DEFAULT NULL,
  `tindakan_nama` varchar(200) DEFAULT NULL,
  `tindakan_unit_id` int(11) DEFAULT NULL,
  `tindakan_kelas_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`tindakan_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `tbl_tindakan` */

/*Table structure for table `tbl_tindakan_pasien` */

DROP TABLE IF EXISTS `tbl_tindakan_pasien`;

CREATE TABLE `tbl_tindakan_pasien` (
  `tindakan_pasien_id` int(11) NOT NULL AUTO_INCREMENT,
  `tindakan_pasien_tgl` date DEFAULT NULL,
  `tp_tindakan_id` int(11) DEFAULT NULL,
  `tp_layanan_id` int(11) DEFAULT NULL,
  `tp_pegawai_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`tindakan_pasien_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `tbl_tindakan_pasien` */

/*Table structure for table `tbl_unit` */

DROP TABLE IF EXISTS `tbl_unit`;

CREATE TABLE `tbl_unit` (
  `unit_id` int(11) NOT NULL AUTO_INCREMENT,
  `unit_nama` varchar(100) DEFAULT NULL,
  `unit_jenlan_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`unit_id`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=latin1;

/*Data for the table `tbl_unit` */

insert  into `tbl_unit`(`unit_id`,`unit_nama`,`unit_jenlan_id`) values (1,'Poli Anak',1),(2,'Poli Bedah',1),(3,'Poli Penyakit Dalam',1),(4,'Poli Gigi dan Mulut',1),(5,'Poli Jantung',1),(6,'Poli Jiwa',1),(7,'Poli Kulit dan Kelamin',1),(8,'Poli Mata',1),(9,'Poli Nyeri',1),(10,'Poli Obgyn',1),(11,'Poli Orthopedi',1),(12,'Poli Paru',1),(13,'Poli Rehab Medik',1),(14,'Poli Saraf',1),(15,'Poli Urologi',1),(16,'Ruang Anak',2),(17,'Ruang Bersalin',2),(18,'Ruang Perinatologi',2),(19,'Ruang Edelweis',2),(20,'Ruang Flamboyan',2),(21,'Ruang Teratai',2),(22,'OK',6);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
