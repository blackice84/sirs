<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('login_model');
    }

    public function index()
    {
        $this->load->view('vlogin');
    }

    public function auth()
    {
        $this->load->model('login_model');

        $user = htmlspecialchars($this->input->post('username', TRUE), ENT_QUOTES);
        $pass = htmlspecialchars($this->input->post('password', TRUE), ENT_QUOTES);
        
        $ceklogin = $this->login_model->auth_user($user, $pass);

        if($ceklogin->num_rows() > 0)
        {
            $data = $ceklogin->row_array();
            //print_r($data);
            $this->session->set_userdata('masuk', TRUE);

            if($data['pegawai_level'] == '1') // Akses Admin
            {
                $this->session->set_userdata('akses','1');
                $this->session->set_userdata('ses_id', $data['pegawai_id']);
                $this->session->set_userdata('ses_nama', $data['pegawai_nama']);
                $this->session->set_userdata('ses_jabatan', $data['pegawai_jabatan']);
                $this->session->set_userdata('ses_unit_id', $data['unit_id']);
                $this->session->set_userdata('ses_unit', $data['unit_nama']);
                $this->session->set_userdata('ses_jenlan_id', $data['jenlan_id']);
                $this->session->set_userdata('ses_layanan', $data['jenlan_nama']);
                redirect('dashboard');

            } 
            elseif($data['pegawai_level'] == '2') // Akses Admisi
            {
                $this->session->set_userdata('akses','2');
                $this->session->set_userdata('ses_id', $data['pegawai_id']);
                $this->session->set_userdata('ses_nama', $data['pegawai_nama']);
                $this->session->set_userdata('ses_jabatan', $data['pegawai_jabatan']);
                $this->session->set_userdata('ses_unit_id', $data['unit_id']);
                $this->session->set_userdata('ses_unit', $data['unit_nama']);
                $this->session->set_userdata('ses_jenlan_id', $data['jenlan_id']);
                $this->session->set_userdata('ses_layanan', $data['jenlan_nama']);
                redirect('registrasi');
            }
            elseif($data['pegawai_level'] == '3') // Akses Perawat
            {
                $this->session->set_userdata('ses_id', $data['pegawai_id']);
                $this->session->set_userdata('ses_nama', $data['pegawai_nama']);
                $this->session->set_userdata('ses_jabatan', $data['pegawai_jabatan']);
                $this->session->set_userdata('ses_unit_id', $data['unit_id']);
                $this->session->set_userdata('ses_unit', $data['unit_nama']);
                $this->session->set_userdata('ses_jenlan_id', $data['jenlan_id']);
                $this->session->set_userdata('ses_layanan', $data['jenlan_nama']);
                redirect('perawat');
            }
            elseif($data['pegawai_level'] == '4') // Akses Laporan
            {
                $this->session->set_userdata('ses_id', $data['pegawai_id']);
                $this->session->set_userdata('ses_nama', $data['pegawai_nama']);
                $this->session->set_userdata('ses_jabatan', $data['pegawai_jabatan']);
                $this->session->set_userdata('ses_unit_id', $data['unit_id']);
                $this->session->set_userdata('ses_unit', $data['unit_nama']);
                $this->session->set_userdata('ses_jenlan_id', $data['jenlan_id']);
                $this->session->set_userdata('ses_layanan', $data['jenlan_nama']);
                redirect('laporan/lapindexdokter');
            }
        }
        else
        {
            $url = base_url();
            echo $this->session->set_flashdata('msg', 'Username atau Password Salah!');
            redirect($url);
        }
    } // Auth End

    public function logout()
    {
        $this->session->sess_destroy();
        redirect('login', 'refresh');
    }


}