<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Perawat extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        // Validasi jika user belum login
        if($this->session->userdata('masuk') != TRUE)
        {
            $url = base_url();
            redirect($url);
        }
    }
    
    public function index()
    {
        $this->load->view('vperawat');
    }

    public function detilpelayanan($id)
    {
        $this->load->model('perawat_model');
        $data['ln'] = $this->perawat_model->detilLayanan($id);
        $data['rwt'] =  $this->perawat_model->riwayatTindakan($id);
        $this->load->view('vdetilpelayananunit', $data);
    }

    public function diagnosa($id)
    {
        $this->load->model('perawat_model');
        $data['diagnosa'] = $this->perawat_model->getDiagnosa();
        $data['lyn'] = $this->perawat_model->getDokDiag($id);
        $data['dokter'] = $this->perawat_model->getDokter();
        $this->load->view('vdiagnosadokter', $data);
    }

    public function updatediagnosa()
    {
        $this->load->model('perawat_model');
        $id = $this->input->post('id_layanan');
        $diag = $this->input->post('diagnosa');
        $dokter = $this->input->post('id_dokter');
        //$data = array(
        //    'dp_diagnosa_id' => $this->input->post('diagnosa'),
            //'dp_layanan_id' => $this->input->post('id_layanan'),
        //    'dp_pegawai_id' => $this->input->post('id_dokter')
        //);
        
        //$data = $this->perawat_model->simpanDokterDiagnosa('tbl_diagnosa_pasien', $data);
        $this->perawat_model->updateStatusLayanan($id);
        $this->perawat_model->updateDiagnosaLayanan($id, $diag, $dokter); 
        redirect('perawat', 'refresh');
    }

    public function krs($id)
    {
        $this->load->model('perawat_model');
        $data['krs'] = $this->perawat_model->getKRS($id);
        $this->load->view('vkrs', $data);
    }

    public function updatekrs()
    {   
        $this->load->model('perawat_model');
        $id = $this->input->post('id_layanan');
        $data = array(
            'layanan_tgl_keluar' => $this->input->post('tgl_krs'),
            'layanan_cara_keluar' => $this->input->post('carakeluar'),
            'layanan_sts_krs' => $this->input->post('stskeluar') 
        );
        $this->perawat_model->updateKRS($id, $data);
        redirect('perawat', 'refresh'); 
    }

    public function pilihkamar($id)
    {
        $this->load->model('perawat_model');
        $data['layanan'] = $this->perawat_model->getLayanan($id);
        $data['kamar'] = $this->perawat_model->getKamar();
        $this->load->view('vpilkamar', $data);
    }

    public function updatekamar()
    {
        $this->load->model('perawat_model');
        $id = $this->input->post('id_layanan');
        $data = array(
            'lay_kamar_id' => $this->input->post('kamar')
        );
        $this->perawat_model->updateKamar($id, $data);
        redirect('perawat', 'refresh'); 
    }
}