<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kelurahan extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        // Validasi jika user belum login
        if($this->session->userdata('masuk') != TRUE)
        {
            $url = base_url();
            redirect($url);
        }
    }

    function index()
    {
        $this->load->model('kelurahan_model');
        $data['kel'] = $this->kelurahan_model->getKelurahan();
        $this->load->view('vdatakelurahan', $data);
    }

    function tambahkelurahan()
    {
        $this->load->model('kelurahan_model');
        $data['kec'] = $this->kelurahan_model->getKecamatan();
        $this->load->view('vkelurahan', $data);
    }

    function simpankel()
    {
        $this->load->model('kelurahan_model');
        $data = array(
            'kelurahan_nama' => $this->input->post('kelurahan'),
            'kelurahan_kec_id' => $this->input->post('kecamatan')
        );

        $data = $this->kelurahan_model->simpanKel('tbl_kelurahan', $data);
        redirect('kelurahan', 'refresh');
    }

    function ubahkel($id)
    {
        $this->load->model('kelurahan_model');
        $data['kel'] = $this->kelurahan_model->viewby($id);
        $data['kec'] = $this->kelurahan_model->getKecamatan();
        $this->load->view('vubahkelurahan', $data);
    }

    function updatekel()
    {
        $this->load->model('kelurahan_model');
        $id = $this->input->post('kel_id');
        $data = array(
            'kelurahan_nama' => $this->input->post('kelurahan'),
            'kelurahan_kec_id' => $this->input->post('kecamatan')
        );

        $this->kelurahan_model->updateKel($id, $data);
        redirect('kelurahan', 'refresh');
    }

}