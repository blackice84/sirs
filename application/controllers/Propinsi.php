<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Propinsi extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        // Validasi jika user belum login
        if($this->session->userdata('masuk') != TRUE)
        {
            $url = base_url();
            redirect($url);
        }
    }

    function index()
    {
        $this->load->model('propinsi_model');
        $data['prop'] = $this->propinsi_model->getPropinsi(); 
        $this->load->view('vdatapropinsi', $data);
    }

    function tambahpropinsi()
    {
        $this->load->view('vpropinsi');
    }

    function simpanpropinsi()
    {
        $this->load->model('propinsi_model');
        $data = array(
            'prop_nama' => $this->input->post('propinsi_nama')
        );

        $data = $this->propinsi_model->simpanPropinsi('tbl_propinsi', $data);
        redirect('propinsi', 'refresh');
    }

    function ubahpropinsi($id)
    {
        $this->load->model('propinsi_model');
        $data['prop'] = $this->propinsi_model->viewby($id);
        $this->load->view('vubahpropinsi', $data);
    }

    function updatepropinsi()
    {
        $this->load->model('propinsi_model');
        $id = $this->input->post('prop_id');
        $data = array(
            'prop_nama' => $this->input->post('propinsi_nama')
        );

        $this->propinsi_model->updatePropinsi($id, $data);
        redirect('propinsi', 'refresh');
    }
}