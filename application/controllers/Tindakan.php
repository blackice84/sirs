<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tindakan extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        // Validasi jika user belum login
        if($this->session->userdata('masuk') != TRUE)
        {
            $url = base_url();
            redirect($url);
        }
    }

    function index()
    {
        $this->load->model('tindakan_model');
        $data['tindakan'] = $this->tindakan_model->getTindakan();
        $this->load->view('vdatatindakan', $data);
    }

    function tambahtindakan()
    {
        $this->load->model('tindakan_model');
        $data['unit'] = $this->tindakan_model->getUnit();
        $data['kelas'] = $this->tindakan_model->getKelas();
        $this->load->view('vtindakan', $data);
    }

    function simpantindakan()
    {
        $this->load->model('tindakan_model');
        $data = array(
            'tindakan_kode' => $this->input->post('kd_tind'),
            'tindakan_nama' => $this->input->post('nama_tind')
        );
        
        $data = $this->tindakan_model->simpanTindakan('tbl_tindakan', $data);
        redirect('tindakan', 'refresh');
    }

    function ubahtindakan($id)
    {
        $this->load->model('tindakan_model');
        $data['tindakan'] = $this->tindakan_model->viewby($id);
        $this->load->view('vubahtindakan', $data);
    }

    function updatetindakan()
    {
        $this->load->model('tindakan_model');
        $id = $this->input->post('id_tind');
        $data = array(
            'tindakan_kode' => $this->input->post('kd_tind'),
            'tindakan_nama' => $this->input->post('nama_tind')
        );

        $this->tindakan_model->updateTindakan($id, $data);
        redirect('tindakan', 'refresh');
    }

    function tindakandokter($id)
    {   
        $this->load->model('tindakan_model');
        $data['tindakan'] = $this->tindakan_model->getTindakan();
        // $data['dokter'] = $this->tindakan_model->getDokter();
        $data['layanan'] = $this->tindakan_model->tindakanLayanan($id);
        $this->load->view('vtindakandokter', $data);
    }

    function simpandoktertindakan()
    {
        $this->load->model('tindakan_model');
        $id = $this->input->post('layanan');
        $data = array(
            'lay_tindakan_id' => $this->input->post('tindakan')
        );

        $this->tindakan_model->simpanDokterTindakan($id, $data);
        
        //$data = $this->tindakan_model->simpanDokterTindakan('tbl_tindakan_pasien', $data);
        //$data1 = $this->tindakan_model->updateStatusLayanan($id); 
        redirect('perawat', 'refresh');
    }

    function hapus_tindakan_pasien($id)
    {
        $this->load->model('tindakan_model');
        $this->tindakan_model->hapusTindakanPasien($id);
        redirect('perawat', 'refresh');
    }

}