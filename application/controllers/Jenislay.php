<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Jenislay extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        // Validasi jika user belum login
        if($this->session->userdata('masuk') != TRUE)
        {
            $url = base_url();
            redirect($url);
        }
    }

    function index()
    {
        $this->load->model('jenlan_model');
        $data['jenlan'] = $this->jenlan_model->getJenlan();
        $this->load->view('vdatajenlay', $data);
    }

    function tambahjenlay()
    {
        $this->load->view('vjenislay');
    }

    function simpanjenlan()
    {
        $this->load->model('jenlan_model');
        $data = array(
            'jenlan_nama' => $this->input->post('jenlan')
        );

        $data = $this->jenlan_model->simpanJenlan('tbl_jenislayanan', $data);
        redirect('jenislay', 'refresh');
    }

    function ubahjenlan($id)
    {
        $this->load->model('jenlan_model');
        $data['jenlan'] = $this->jenlan_model->viewby($id);
        $this->load->view('vubahjenlan', $data);
    }

    function updatejenlan()
    {
        $this->load->model('jenlan_model');
        $id = $this->input->post('jenlan_id');
        $data = array(
            'jenlan_nama' => $this->input->post('jenlan_nama')
        );

        $this->jenlan_model->updateJenlan($id, $data);
        redirect('jenislay', 'refresh');
    }
}