<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kamar extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        // Validasi jika user belum login
        if($this->session->userdata('masuk') != TRUE)
        {
            $url = base_url();
            redirect($url);
        }
    }

    function index()
    {
        $this->load->model('kamar_model');
        $data['kamar'] = $this->kamar_model->getKamar();
        $this->load->view('vdatakamar', $data);
    }

    function tambahkamar()
    {
        $this->load->model('kamar_model');
        $data['kelas'] = $this->kamar_model->getKelas();
        $this->load->view('vkamar', $data);
    }

    function simpankamar()
    {
        $this->load->model('kamar_model');
        $data = array(
            'kamar_ruangan' => $this->input->post('kamar'),
            'kamar_bed' => $this->input->post('bed')
        );

        $data = $this->kamar_model->simpanKamar('tbl_kamar', $data);
        redirect('kamar', 'refresh');
    }

    function ubahkamar($id)
    {
        $this->load->model('kamar_model');
        $data['kamar'] = $this->kamar_model->viewby($id);
        $data['kelas'] = $this->kamar_model->getKelas(); 
        $this->load->view('vubahkamar', $data);
    }

    function updatekamar()
    {
        $this->load->model('kamar_model');
        $id = $this->input->post('kamar_id');
        $data = array(
            'kamar_ruangan' => $this->input->post('kamar'),
            'kamar_bed' => $this->input->post('bed'),
            'kamar_kelas_id' => $this->input->post('kelas')
        );

        $data = $this->kamar_model->updateKamar($id, $data);
        redirect('kamar', 'refresh');
    }

}