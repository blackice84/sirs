<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Registrasi extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();

        $this->load->model('registrasi_model');

        if($this->session->userdata('masuk') != TRUE)
        {
            $url = base_url();
            redirect($url);
        }
    }

    public function index()
    {
        $data['jenlan'] = $this->registrasi_model->getJenisLayanan();
        $data['kelas'] = $this->registrasi_model->getKelas();
        //$data['diagnosa'] = $this->registrasi_model->getDiagnosa();
        $this->load->view('vregistrasi', $data);
    }

    public function get_mr_code()
    {
        $data = $this->registrasi_model->get_no_rm();
        echo json_encode($data);
    }

    public function getunit()
    {
        $id = $this->input->post('id');
        $data = $this->registrasi_model->getTempatLayanan($id);
        echo json_encode($data);
    }

    public function getpasien()
    {
        $id = $this->input->post('norm');
        $data = $this->registrasi_model->getPasien($id);
        echo json_encode($data);
    }

    function simpanreg()
    {
        $data = array(
            'layanan_tgl_masuk' => date('Y-m-d'),
            'layanan_status' => '0',
            'lay_jenlan_id' => $this->input->post('jenlan'),
            'lay_unit_id' => $this->input->post('tmplay'),
            'lay_pasien_id' => $this->input->post('pasien_id'),
            'lay_diagnosa_id' => '15',
            'lay_kelas_id' => $this->input->post('kelas'),
            'lay_dokter_id' => '2',
            'lay_kamar_id' => '14',
            'lay_tindakan_id' => '43'
        );

        $data = $this->registrasi_model->simpanDaftar('tbl_layanan', $data);
        redirect('registrasi/kunjungan', 'refresh');
    }

    function kunjungan()
    {
        $data['kunj'] = $this->registrasi_model->lihatKunjungan();
        $this->load->view('vkunjungan', $data);
    }

}