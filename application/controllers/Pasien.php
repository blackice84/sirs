<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pasien extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        // Validasi jika user belum login
        if($this->session->userdata('masuk') != TRUE)
        {
            $url = base_url();
            redirect($url);
        }
    }

    function index()
    {
        $this->load->model('pasien_model');
        $data['pasien'] = $this->pasien_model->getPasien();
        $this->load->view('vdatapasien', $data);
    }

    function tambahpasien()
    {
        $this->load->model('pasien_model');
        $data['prop'] = $this->pasien_model->getPropinsi();
        $this->load->view('vpasien', $data);
    }

    function kab()
    {
        $this->load->model('pasien_model');
        $id = $this->input->post('id_prop');
        $data['kab'] = $this->pasien_model->getKabupaten($id);
        $this->load->view('vkab', $data);
    }

    function kec()
    {
        $this->load->model('pasien_model');
        $id = $this->input->post('id_kab');
        $data['kec'] = $this->pasien_model->getKecamatan($id);
        $this->load->view('vkec', $data);
    }

    function simpanpasien()
    {
        $this->load->model('pasien_model');
        $data = array(
            'pasien_norm' => $this->input->post('no_rm'),
            'pasien_nik' => $this->input->post('nik'),
            'pasien_nama' => $this->input->post('nama_pasien'),
            'pasien_jenkel' => $this->input->post('jenkel'),
            'pasien_tlahir' => $this->input->post('tmp_lahir'),
            'pasien_dlahir' => $this->input->post('tgl_lahir'),
            'pasien_agama' => $this->input->post('agama'),
            'pasien_prop_id' => $this->input->post('propinsi'),
            'pasien_kabupaten_id' => $this->input->post('kabupaten'),
            'pasien_kecamatan_id' => $this->input->post('kecamatan'),
            'pasien_kelurahan_id' => $this->input->post('kelurahan'),
            'pasien_alamat' => $this->input->post('alamat'),
            'pasien_rt' => $this->input->post('rt'),
            'pasien_rw' => $this->input->post('rw'),
            'pasien_telp' => $this->input->post('telp'),
            'pasien_pekerjaan' => $this->input->post('pekerjaan'),
            'pasien_pendidikan' => $this->input->post('pendidikan')
        );

        $data = $this->pasien_model->simpanpasien('tbl_pasien', $data);
        redirect('pasien', 'refresh');
    }

}