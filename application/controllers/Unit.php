<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Unit extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        // Validasi jika user belum login
        if($this->session->userdata('masuk') != TRUE)
        {
            $url = base_url();
            redirect($url);
        }
    }

    function index()
    {
        $this->load->model('unit_model');
        $data['unit'] = $this->unit_model->getUnit();
        $this->load->view('vdataunit', $data);
    }

    function tambahunit()
    {   
        $this->load->model('unit_model');
        $data['layanan'] = $this->unit_model->getLayanan();
        $this->load->view('vunit', $data);
    }

    function simpanunit()
    {
        $this->load->model('unit_model');
        $data = array(
            'unit_nama' => $this->input->post('unit_nama'),
            'unit_jenlan_id' => $this->input->post('jenlay')
        );

        $data = $this->unit_model->createunit('tbl_unit', $data);
        redirect('unit', 'refresh');
    }

    function ubahunit($id)
    {
        $this->load->model('unit_model');
        $data['unit'] = $this->unit_model->viewby($id);
        $data['layanan'] = $this->unit_model->getLayanan();
        $this->load->view('vubahunit', $data);
    }

    function updateunit()
    {
        $this->load->model('unit_model');
        $id = $this->input->post('unit_id');
        $data = array(
            'unit_nama' => $this->input->post('unit_nama'),
            'unit_jenlan_id' => $this->input->post('jenlay')
        );

        $this->unit_model->updateUnit($id, $data);
        redirect('unit', 'refresh');
    }

}