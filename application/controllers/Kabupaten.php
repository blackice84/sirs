<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kabupaten extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        // Validasi jika user belum login
        if($this->session->userdata('masuk') != TRUE)
        {
            $url = base_url();
            redirect($url);
        }
    }

    function index()
    {
        $this->load->model('kabupaten_model');
        $data['kab'] = $this->kabupaten_model->getKabupaten();
        $this->load->view('vdatakabupaten', $data);
    }

    function tambahkabupaten()
    {
        $this->load->model('kabupaten_model');
        $data['prop'] = $this->kabupaten_model->getPropinsi();
        $this->load->view('vkabupaten', $data);
    }

    function simpankab()
    {
        $this->load->model('kabupaten_model');
        $data = array(
            'kabupaten_nama' => $this->input->post('kabupaten_nama'),
            'kabupaten_prop_id' => $this->input->post('propinsi')
        );

        $data = $this->kabupaten_model->simpanKab('tbl_kabupaten', $data);
        redirect('kabupaten', 'refresh');
    }

    function ubahkab($id)
    {
        $this->load->model('kabupaten_model');
        $data['kab'] = $this->kabupaten_model->viewby($id);
        $data['prop'] = $this->kabupaten_model->getPropinsi();
        $this->load->view('vubahkabupaten', $data);
    }

    function updatekab()
    {
        $this->load->model('kabupaten_model');
        $id = $this->input->post('kab_id');
        $data = array(
            'kabupaten_nama' => $this->input->post('kabupaten_nama'),
            'kabupaten_prop_id' => $this->input->post('propinsi')
        );

        $this->kabupaten_model->updateKab($id, $data);
        redirect('kabupaten', 'refresh');
    }
}