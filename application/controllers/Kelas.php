<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kelas extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        // Validasi jika user belum login
        if($this->session->userdata('masuk') != TRUE)
        {
            $url = base_url();
            redirect($url);
        }
    }

    function index()
    {
        $this->load->model('kelas_model');
        $data['kelas'] = $this->kelas_model->getKelas(); 
        $this->load->view('vdatakelas', $data);
    }

    function tambahkelas()
    {
        $this->load->view('vkelas');
    }

    function simpankelas()
    {
        $this->load->model('kelas_model');
        $data = array(
            'kelas_nama' => $this->input->post('kelas')
        );

        $data = $this->kelas_model->simpanKelas('tbl_kelas', $data);
        redirect('kelas', 'refresh');
    }

    function ubahkelas($id)
    {
        $this->load->model('kelas_model');
        $data['kelas'] = $this->kelas_model->viewby($id);
        $this->load->view('vubahkelas', $data);
    }

    function updatekelas()
    {
        $this->load->model('kelas_model');
        $id = $this->input->post('id_kelas');
        $data = array(
            'kelas_nama' => $this->input->post('kelas')
        );

        $this->kelas_model->updateKelas($id, $data);
        redirect('kelas', 'refresh');
    }
}