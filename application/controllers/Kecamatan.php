<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kecamatan extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        // Validasi jika user belum login
        if($this->session->userdata('masuk') != TRUE)
        {
            $url = base_url();
            redirect($url);
        }
    }

    function index()
    {
        $this->load->model('kecamatan_model');
        $data['kec'] = $this->kecamatan_model->getKecamatan();
        $this->load->view('vdatakecamatan', $data);
    }

    function tambahkecamatan()
    {
        $this->load->model('kecamatan_model');
        $data['kab'] = $this->kecamatan_model->getKabupaten();
        $this->load->view('vkecamatan', $data);
    }

    function simpankec()
    {
        $this->load->model('kecamatan_model');
        $data = array(
            'kecamatan_nama' => $this->input->post('kecamatan_nama'),
            'kecamatan_kab_id' => $this->input->post('kabupaten')
        );

        $data = $this->kecamatan_model->simpanKec('tbl_kecamatan', $data);
        redirect('kecamatan', 'refresh');
    }

    function ubahkec($id)
    {
        $this->load->model('kecamatan_model');
        $data['kec'] = $this->kecamatan_model->viewby($id);
        $data['kbp'] = $this->kecamatan_model->getKabupaten();
        $this->load->view('vubahkec', $data);
    }

    function updatekec()
    {
        $this->load->model('kecamatan_model');
        $id = $this->input->post('kec_id');
        $data = array(
            'kecamatan_nama' => $this->input->post('kecamatan'),
            'kecamatan_kab_id' => $this->input->post('kabupaten')
        );
        
        //print_r($data);
        $this->kecamatan_model->updateKec($id, $data);
        redirect('kecamatan', 'refresh');
    }

}