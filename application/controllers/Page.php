<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Page extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        // Validasi jika user belum login
        if($this->session->userdata('masuk') != TRUE)
        {
            $url = base_url();
            redirect($url);
        }
    }

    function index()
    {
        echo 'Anda berhasil Masuk di Page';
    }
}