<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Laporan extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        // Validasi jika user belum login
        if($this->session->userdata('masuk') != TRUE)
        {
            $url = base_url();
            redirect($url);
        }
    }

    public function index()
    {
        $this->load->view('vlaporan');
    }

    public function lapindexdokter()
    {
        $this->load->model('laporan_model');
        $data['dkt'] = $this->laporan_model->getDokter();
        $this->load->view('vlapindexdokter', $data);
    }

    public function getindexdokter()
    {
        $this->load->model('laporan_model');
        $d = $this->input->post('dokter');
        $t1 = $this->input->post('dp1');
        $t2 = $this->input->post('dp2');

        $data['idk'] = $this->laporan_model->getIndexDokter($d, $t1, $t2);
        $this->load->view('vhasilindexdokter', $data);
    }

    public function lapcetakindexdokter()
    {
        $this->load->model('laporan_model');
        $data['dkt'] = $this->laporan_model->getDokter();
        $this->load->view('vreportexcelindexdokter', $data);
    }

    public function lapexcel()
    {
        $this->load->model('laporan_model');
        $d = $this->input->post('dokter');
        $t1 = $this->input->post('dp3');
        $t2 = $this->input->post('dp4');
        header("Content-type=application/vnd.ms-excel");
        header("Content-disposition:attachment; filename=Laporan_index_dokter.xls");
        $data['idk'] = $this->laporan_model->getIndexDokter($d, $t1, $t2);
        $this->load->view('vexcelindexdokter', $data);
    }

    public function lapindexpenyakit()
    {
        $this->load->model('laporan_model');
        $data['pykt'] = $this->laporan_model->getDiagnosa();
        $this->load->view('vlapindexpenyakit', $data);
    }

    public function getindexpenyakit()
    {
        $this->load->model('laporan_model');
        $d = $this->input->post('penyakit');
        $t1 = $this->input->post('dp1');
        $t2 = $this->input->post('dp2');

        $data['ipk'] = $this->laporan_model->getIndexDiagnosa($d, $t1, $t2);
        $this->load->view('vhasilindexpenyakit', $data);
    }

    public function lapcetakindexpenyakit()
    {
        $this->load->model('laporan_model');
        $data['dg'] = $this->laporan_model->getDiagnosa();
        $this->load->view('vreportexcelindexpenyakit', $data);
    }

    public function getlappenyakit()
    {
        $this->load->model('laporan_model');
        $d = $this->input->post('penyakit');
        $t1 = $this->input->post('dp1');
        $t2 = $this->input->post('dp2');
        header("Content-type=application/vnd.ms-excel");
        header("Content-disposition:attachment; filename=Laporan_Index_Penyakit.xls");
        $data['gid'] = $this->laporan_model->getIndexDiagnosa($d, $t1, $t2);
        $this->load->view('vexcelindexpenyakit', $data);
    }

    public function lapindexmati()
    {
        $this->load->model('laporan_model');
        $data['dga'] = $this->laporan_model->getDiagnosa();
        $this->load->view('vlapindexmati', $data);
    }

    public function getindexmati()
    {
        $this->load->model('laporan_model');
        $d = $this->input->post('penyakit');
        $t1 = $this->input->post('dp1');
        $t2 = $this->input->post('dp2');

        $data['idm'] = $this->laporan_model->getIndexDeath($d, $t1, $t2);
        $this->load->view('vhasilindexmati', $data);
    }

    public function lapcetakindexmati()
    {
        $this->load->model('laporan_model');
        $data['dga'] = $this->laporan_model->getDiagnosa();
        $this->load->view('vreportexcelindexmati', $data);
    }

    public function getlapmati()
    {
        $this->load->model('laporan_model');
        $d = $this->input->post('penyakit');
        $t1 = $this->input->post('dp1');
        $t2 = $this->input->post('dp2');
        header("Content-type=application/vnd.ms-excel");
        header("Content-disposition:attachment; filename=Laporan_Index_Kematian.xls");
        $data['gim'] = $this->laporan_model->getIndexDeath($d, $t1, $t2);
        $this->load->view('vexcelindexmati', $data);
    }

    public function lapindextindakan()
    {
        $this->load->model('laporan_model');
        $data['tind'] = $this->laporan_model->getTindakan();
        $this->load->view('vlapindextindakanop', $data);
    }

    public function getindextind()
    {
        $this->load->model('laporan_model');
        $d = $this->input->post('tindakan');
        $t1 = $this->input->post('dp1');
        $t2 = $this->input->post('dp2');

        $data['tink'] = $this->laporan_model->getIndexTindakan($d, $t1, $t2);
        $this->load->view('vhasilindextind', $data);
    }

    public function grafikdokter()
    {
        $this->load->model('laporan_model');
        $x['data'] = $this->laporan_model->getDataGrafik();
        $this->load->view('vgrafikdokter', $x);
    }

    public function lapcetakindextind()
    {
        $this->load->model('laporan_model');
        $data['tind'] = $this->laporan_model->getTindakan();
        $this->load->view('vreportexcelindextind', $data);
    }

    public function getlaptind()
    {
        $this->load->model('laporan_model');
        $d = $this->input->post('tindakan');
        $t1 = $this->input->post('dp1');
        $t2 = $this->input->post('dp2');
        header("Content-type=application/vnd.ms-excel");
        header("Content-disposition:attachment; filename=Laporan_Index_Tindakan.xls");
        $data['tink'] = $this->laporan_model->getIndexTindakan($d, $t1, $t2);
        $this->load->view('vexcelindextind', $data);
    }

    public function laptenindex()
    {
        $this->load->view('vlaptenindex');
    }

    public function lapcetakindexten()
    {
        $this->load->model('laporan_model');
        $u1 = $this->input->post('umura');
        $u2 = $this->input->post('umurb');
        header("Content-type=application/vnd.ms-excel");
        header("Content-disposition:attachment; filename=Laporan_10_besar_penyakit.xls");
        $data['ten'] = $this->laporan_model->getTenIndex($u1, $u2);
        $this->load->view('vexcelten', $data);
    }

    public function lapindexpenyakitall()
    {
        $this->load->view('vlapindexall');
    }

    public function getindexpenyakitall()
    {
        $this->load->model('laporan_model');

        $t1 = $this->input->post('dp1');
        $t2 = $this->input->post('dp2');
        header("Content-type=application/vnd.ms-excel");
        header("Content-disposition:attachment; filename=Laporan_semua_penyakit.xls");
        $data['pall'] = $this->laporan_model->getIndexDiagnosaAll($t1, $t2);
        $this->load->view('vexcelindexpall', $data);
    }

    public function lapindexmatiall()
    {
        $this->load->view('vlapmatiall');
    }

    public function getindexmatiall()
    {
        $this->load->model('laporan_model');

        $t1 = $this->input->post('dp1');
        $t2 = $this->input->post('dp2');
        header("Content-type=application/vnd.ms-excel");
        header("Content-disposition:attachment; filename=Laporan_semua_kematian.xls");
        $data['mall'] = $this->laporan_model->getIndexMatiAll($t1, $t2);
        $this->load->view('vexcelindexmatiall', $data);
    }

    public function lapindextindall()
    {
        $this->load->view('vlaptindall');
    }

    public function getindextindall()
    {
        $this->load->model('laporan_model');

        $t1 = $this->input->post('dp1');
        $t2 = $this->input->post('dp2');
        header("Content-type=application/vnd.ms-excel");
        header("Content-disposition:attachment; filename=Laporan_semua_tindakan.xls");
        $data['tall'] = $this->laporan_model->getIndexTindAll($t1, $t2);
        $this->load->view('vexcelindextindall', $data);
    }

    public function lapindexdokterall()
    {
        $this->load->view('vlapdokterall');
    }

    public function getindexdokterall()
    {
        $this->load->model('laporan_model');
        //$d = $this->input->post('dokter');
        $t1 = $this->input->post('dp1');
        $t2 = $this->input->post('dp2');
        header("Content-type=application/vnd.ms-excel");
        header("Content-disposition:attachment; filename=Laporan_index_semua_dokter.xls");
        $data['dall'] = $this->laporan_model->getIndexDokterAll($t1, $t2);
        $this->load->view('vexcelindexdokterall', $data);
    }

}