<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Diagnosa extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        // Validasi jika user belum login
        if($this->session->userdata('masuk') != TRUE)
        {
            $url = base_url();
            redirect($url);
        }
    }

    function index()
    {
        $this->load->model('diagnosa_model');
        $data['diagnosa'] = $this->diagnosa_model->getDiagnosa();
        $this->load->view('vdatadiagnosa', $data);
    }

    function tambahdiagnosa()
    {
        $this->load->view('vdiagnosa');
    }

    function simpandiagnosa()
    {
        $this->load->model('diagnosa_model');
        $data = array(
            'diagnosa_kode' => $this->input->post('kd_diag'),
            'diagnosa_nama' => $this->input->post('nama_diag')
        );

        $data = $this->diagnosa_model->createDiag('tbl_diagnosa', $data);
        redirect('diagnosa', 'refresh');
    }

    function ubahdiagnosa($id)
    {
        $this->load->model('diagnosa_model');
        $data['diagnosa'] = $this->diagnosa_model->viewby($id);
        $this->load->view('vubahdiagnosa', $data);
    }

    function updatediagnosa()
    {
        $this->load->model('diagnosa_model');
        $id = $this->input->post('id_diag');
        $data = array(
            'diagnosa_kode' => $this->input->post('kd_diag'),
            'diagnosa_nama' => $this->input->post('nama_diag')
        );

        $this->diagnosa_model->updateDiagnosa($id, $data);
        redirect('diagnosa', 'refresh');
    }
}