<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pegawai extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        // Validasi jika user belum login
        if($this->session->userdata('masuk') != TRUE)
        {
            $url = base_url();
            redirect($url);
        }
    }

    function index()
    {
        $this->load->model('pegawai_model');
        $data['pegawai'] = $this->pegawai_model->getPegawai();
        $this->load->view('vdatapegawai', $data);
    }

    function tambahpegawai()
    {   
        $this->load->model('pegawai_model');
        $data['unit'] = $this->pegawai_model->getUnit();
        $this->load->view('vpegawai', $data);
    }

    function simpanpegawai()
    {
        $this->load->model('pegawai_model');
        $data = array(
            'pegawai_nama' => $this->input->post('nama_peg'),
            'pegawai_alamat' => $this->input->post('alamat_peg'),
            'pegawai_telp' => $this->input->post('telp_peg'),
            'pegawai_user' => $this->input->post('user_peg'),
            'pegawai_pass' => md5($this->input->post('pass_peg')),
            'pegawai_level' => $this->input->post('level_peg'),
            'pegawai_jabatan' => $this->input->post('jabatan_peg'),
            'pegawai_unit_id' => $this->input->post('unit_peg')
        );

        $data = $this->pegawai_model->createpeg('tbl_pegawai', $data);
        redirect('pegawai', 'refresh');
    }

    function ubahpegawai($id)
    {   
        $this->load->model('pegawai_model');
        $data['pegawai'] = $this->pegawai_model->viewby($id);
        $data['unit'] = $this->pegawai_model->getUnit();
        $this->load->view('vubahpegawai', $data);
    }

    function updatepegawai()
    {
        $this->load->model('pegawai_model');
        $id = $this->input->post('id_peg');
        $data = array(
            'pegawai_nama' => $this->input->post('nama_peg'),
            'pegawai_alamat' => $this->input->post('alamat_peg'),
            'pegawai_telp' => $this->input->post('telp_peg'),
            'pegawai_unit_id' => $this->input->post('unit_peg')
        );

        $this->pegawai_model->updatePegawai($id, $data);
        redirect('pegawai', 'refresh');
    }
}