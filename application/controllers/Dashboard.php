<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        // Validasi jika user belum login
        if($this->session->userdata('masuk') != TRUE)
        {
            $url = base_url();
            redirect($url);
        }
    }

    public function index()
    {
        $this->load->model('dashboard_model');
        $data['totrj'] = $this->dashboard_model->jumlahPRJ();
        $data['totri'] = $this->dashboard_model->jumlahPRI();
        $data['totkrs'] = $this->dashboard_model->jumlahKRS();
        $data['totpsm'] = $this->dashboard_model->jumlahPM();
        $data['tendiag'] = $this->dashboard_model->tenDiagnose();
        $data['tentind'] = $this->dashboard_model->tenTindakan();
        $data['tentd'] = $this->dashboard_model->tenDeseaseDeath();
        $data['x'] = $this->dashboard_model->getDataGrafik();
        $this->load->view('vdashboard', $data);
    }

    public function grafikdokter()
    {
        $this->load->model('dashboard_model');
        $x['data'] = $this->laporan_model->getDataGrafik();
        $this->load->view('vdashboard', $x);
    }

    public function smt2()
    {
        $this->load->model('dashboard_model');
        $data['totrj2'] = $this->dashboard_model->jumlahPRJ2();
        $data['totri2'] = $this->dashboard_model->jumlahPRI2();
        $data['totkrs2'] = $this->dashboard_model->jumlahKRS2();
        $data['totpsm2'] = $this->dashboard_model->jumlahPM2();
        $data['tendiag2'] = $this->dashboard_model->tenDiagnose2();
        $data['tentind2'] = $this->dashboard_model->tenTindakan2();
        $data['tentd2'] = $this->dashboard_model->tenDeseaseDeath2();
        $data['x2'] = $this->dashboard_model->getDataGrafik2();
        $this->load->view('vdashboards', $data);
    }
    
    public function grafikdokter2()
    {
        $this->load->model('dashboard_model');
        $x2['data'] = $this->laporan_model->getDataGrafik2();
        $this->load->view('vdashboard', $x2);
    }
}