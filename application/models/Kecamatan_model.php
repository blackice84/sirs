<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kecamatan_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function getKabupaten()
    {
        $qry = $this->db->get('tbl_kabupaten');
        return $qry->result();
    }

    public function getKecamatan()
    {
        // Query Join
        $this->db->select('*');
        $this->db->from('tbl_kecamatan');
        $this->db->join('tbl_kabupaten', 'tbl_kabupaten.kabupaten_id = tbl_kecamatan.kecamatan_kab_id');
        $qry = $this->db->get();
        return $qry->result();
    }

    public function simpanKec($table, $data)
    {
        $res = $this->db->insert($table, $data);
        return $res;
    }

    public function viewby($id)
    {
        $qry = $this->db->query("
            SELECT kc.kecamatan_id, kc.kecamatan_nama, kc.kecamatan_kab_id,
            kb.kabupaten_id, kb.kabupaten_nama
            FROM tbl_kecamatan kc, tbl_kabupaten kb
            WHERE kc.kecamatan_kab_id = kb.kabupaten_id AND kc.kecamatan_id = '$id' 
        ");
        
        return $qry->row();
    }

    public function updateKec($id, $data)
    {
        $this->db->where('kecamatan_id', $id);
        return $this->db->update('tbl_kecamatan', $data);
    }
}