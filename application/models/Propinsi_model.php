<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Propinsi_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function getPropinsi()
    {
        $qry = $this->db->get('tbl_propinsi');
        return $qry->result();
    }

    public function simpanPropinsi($table, $data)
    {
        $qry = $this->db->insert($table, $data);
        return $qry;
    }

    public function viewby($id)
    {
        $qry = $this->db->query("SELECT * FROM tbl_propinsi WHERE prop_id = '$id'");
        return $qry->row();
    }

    public function updatePropinsi($id, $data)
    {
        $this->db->where('prop_id', $id);
        return $this->db->update('tbl_propinsi', $data);
    }
}