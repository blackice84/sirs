<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Perawat_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function detilLayanan($id)
    {
        $qry = $this->db->query("SELECT tbl_layanan.layanan_id, tbl_layanan.layanan_tgl_masuk, tbl_pasien.pasien_norm, tbl_pasien.pasien_nama, tbl_kelas.kelas_nama,
        tbl_unit.unit_nama, tbl_jenislayanan.jenlan_nama, tbl_layanan.layanan_tgl_keluar, tbl_layanan.layanan_status, tbl_kamar.kamar_ruangan, tbl_kamar.kamar_bed
        FROM tbl_layanan JOIN tbl_pasien ON tbl_layanan.lay_pasien_id = tbl_pasien.pasien_id
        JOIN tbl_kelas ON tbl_layanan.lay_kelas_id = tbl_kelas.kelas_id
        JOIN tbl_jenislayanan ON tbl_layanan.lay_jenlan_id = tbl_jenislayanan.jenlan_id
        JOIN tbl_unit ON tbl_layanan.lay_unit_id = tbl_unit.unit_id
        JOIN tbl_kamar ON tbl_layanan.lay_kamar_id = tbl_kamar.kamar_id
        WHERE tbl_layanan.layanan_id = '$id'");

        return $qry->row();
    }

    public function riwayatTindakan($id)
    {
        $rt = $this->db->query("SELECT tbl_tindakan.tindakan_kode, tbl_tindakan.tindakan_nama
                                FROM tbl_layanan JOIN tbl_tindakan ON tbl_layanan.lay_tindakan_id = tbl_tindakan.tindakan_id
                                WHERE tbl_layanan.layanan_id = '$id'");
        return $rt->result();
    }

    public function getDiagnosa()
    {
        $qry = $this->db->get('tbl_diagnosa');
        return $qry->result();
    }

    public function getDokDiag($id)
    {
        $dcdiag = $this->db->query("SELECT layanan_id FROM tbl_layanan WHERE layanan_id = '$id'");
        return $dcdiag->row();
    }

    public function updateStatusLayanan($id)
    {
        $qryup = $this->db->query("UPDATE tbl_layanan SET layanan_status = '1' WHERE layanan_id = '$id'");
        return $qryup;
    }

    public function getDokter()
    {
        $dktr = $this->db->query("SELECT * FROM tbl_pegawai WHERE pegawai_jabatan LIKE 'Dokter%'");
        return $dktr->result();
    }

    public function simpanDokterDiagnosa($table, $data)
    {
        $this->db->insert($table, $data);
        $insert_id = $this->db->insert_id();
        
        return $insert_id;
    }

    public function updateDiagnosaLayanan($id, $diag, $dokter)
    {
        $qry = $this->db->query("UPDATE tbl_layanan SET lay_diagnosa_id = '$diag', lay_dokter_id = '$dokter' WHERE layanan_id = '$id'");
        return $qry;
    }

    public function getKRS($id)
    {
        $krs = $this->db->query("SELECT * FROM tbl_layanan WHERE layanan_id = '$id'");
        return $krs->row();
    }

    public function updateKRS($id, $data)
    {
        $this->db->where('layanan_id', $id);
        return $this->db->update('tbl_layanan', $data);
    }

    public function getLayanan($id)
    {
        $kamar = $this->db->query("SELECT * FROM tbl_layanan WHERE layanan_id = '$id'");
        return $kamar->row();
    }

    public function getKamar()
    {
        $kmr = $this->db->get('tbl_kamar');
        return $kmr->result();
    }

    public function updateKamar($id, $data)
    {
        $this->db->where('layanan_id', $id);
        return $this->db->update('tbl_layanan', $data);
    }
}