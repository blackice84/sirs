<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Unit_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function createunit($table, $data)
    {
        $res = $this->db->insert($table, $data);
        return $res;
    }

    public function getUnit()
    {
        // Query Join
        $this->db->select('*');
        $this->db->from('tbl_unit');
        $this->db->join('tbl_jenislayanan', 'tbl_jenislayanan.jenlan_id = tbl_unit.unit_jenlan_id');
        $qry = $this->db->get();
        return $qry->result();
    }

    public function getLayanan()
    {
        $qry = $this->db->get('tbl_jenislayanan');
        return $qry->result();
    }

    public function viewby($id)
    {
        $qry = $this->db->query("SELECT u.unit_id, u.unit_nama, u.unit_jenlan_id, j.jenlan_id, j.jenlan_nama
               FROM tbl_unit u, tbl_jenislayanan j
               WHERE u.unit_jenlan_id = j.jenlan_id AND u.unit_id = '$id'");

        return $qry->row();
    }

    public function updateUnit($id, $data)
    {
        $this->db->where('unit_id', $id);
        return $this->db->update('tbl_unit', $data);
    }

}