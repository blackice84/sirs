<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Tindakan_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function createtind($table, $data)
    {
        $res = $this->db->insert($table, $data);
        return $res;
    }

    public function getTindakan()
    {
        $qry = $this->db->get('tbl_tindakan');
        return $qry->result();
    }

    public function getUnit()
    {
        $qry = $this->db->get('tbl_unit');
        return $qry->result();
    }

    public function getKelas()
    {
        $qry = $this->db->get('tbl_kelas');
        return $qry->result();
    }

    public function simpanTindakan($table, $data)
    {
        $qry = $this->db->insert($table, $data);
        return $qry;
    }

    public function viewby($id)
    {
        $qry = $this->db->query("SELECT * FROM tbl_tindakan WHERE tindakan_id = '$id'");
        return $qry->row();
    }

    public function updateTindakan($id, $data)
    {
        $this->db->where('tindakan_id', $id);
        return $this->db->update('tbl_tindakan', $data);
    }

    public function tindakanLayanan($id)
    {
        $qry = $this->db->query("SELECT tbl_layanan.layanan_id, tbl_layanan.layanan_tgl_masuk, tbl_pasien.pasien_norm, tbl_pasien.pasien_nama, tbl_kelas.kelas_nama,
        tbl_unit.unit_nama, tbl_jenislayanan.jenlan_nama, tbl_layanan.layanan_tgl_keluar, tbl_layanan.layanan_status
        FROM tbl_layanan JOIN tbl_pasien ON tbl_layanan.lay_pasien_id = tbl_pasien.pasien_id
        JOIN tbl_kelas ON tbl_layanan.lay_kelas_id = tbl_kelas.kelas_id
        JOIN tbl_jenislayanan ON tbl_layanan.lay_jenlan_id = tbl_jenislayanan.jenlan_id
        JOIN tbl_unit ON tbl_layanan.lay_unit_id = tbl_unit.unit_id
        WHERE tbl_layanan.layanan_id = '$id'");
        return $qry->row();
    }

    public function getDokter()
    {
        $dktr = $this->db->query("SELECT * FROM tbl_pegawai WHERE pegawai_jabatan LIKE 'Dokter%'");
        return $dktr->result();
    }

    public function simpanDokterTindakan($id, $data)
    {
        $this->db->where('layanan_id', $id);
        return $this->db->update('tbl_layanan', $data);
    }

    public function updateStatusLayanan($id)
    {
        $qryup = $this->db->query("UPDATE tbl_layanan SET layanan_status = '1' WHERE layanan_id = '$id'");
        return $qryup;
    }

    public function hapusTindakanPasien($id)
    {
        $this->db->where('tindakan_pasien_id', $id);
        $this->db->delete('tbl_tindakan_pasien');
    }

    
}