<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kabupaten_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function getPropinsi()
    {
        $qry = $this->db->get('tbl_propinsi');
        return $qry->result();
    }

    public function getKabupaten()
    {
        // Query Join
        $this->db->select('*');
        $this->db->from('tbl_kabupaten');
        $this->db->join('tbl_propinsi', 'tbl_propinsi.prop_id = tbl_kabupaten.kabupaten_prop_id');
        $qry = $this->db->get();
        return $qry->result();
    }

    public function simpanKab($table, $data)
    {
        $res = $this->db->insert($table, $data);
        return $res;
    }

    public function viewby($id)
    {
        $qry = $this->db->query("
            SELECT tbl_kabupaten.kabupaten_id, tbl_kabupaten.kabupaten_nama, tbl_kabupaten.kabupaten_prop_id,
            tbl_propinsi.prop_id, tbl_propinsi.prop_nama
            FROM tbl_kabupaten, tbl_propinsi
            WHERE tbl_kabupaten.kabupaten_prop_id = tbl_propinsi.prop_id AND
            tbl_kabupaten.kabupaten_id = '$id' 
        ");
        
        return $qry->row();
    }

    public function updateKab($id, $data)
    {
        $this->db->where('kabupaten_id', $id);
        return $this->db->update('tbl_kabupaten', $data);
    }
}