<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Laporan_model extends CI_Model
{
    public function getIndexDokter($d, $t1, $t2)
    {
        $qry = $this->db->query("SELECT tbl_layanan.layanan_id, tbl_pasien.pasien_norm, tbl_pasien.pasien_nama, tbl_pasien.pasien_dlahir, 
        tbl_unit.unit_nama, tbl_kelas.kelas_nama, tbl_layanan.layanan_tgl_masuk, 
        tbl_layanan.layanan_tgl_keluar, tbl_diagnosa.diagnosa_kode, tbl_diagnosa.diagnosa_nama,
        tbl_layanan.layanan_cara_keluar, tbl_pegawai.pegawai_nama
        FROM   tbl_layanan
        JOIN tbl_pasien ON tbl_layanan.lay_pasien_id = tbl_pasien.pasien_id
        JOIN tbl_unit ON tbl_layanan.lay_unit_id = tbl_unit.unit_id
        JOIN tbl_kelas ON tbl_layanan.lay_kelas_id = tbl_kelas.kelas_id
        JOIN tbl_diagnosa ON tbl_layanan.lay_diagnosa_id = tbl_diagnosa.diagnosa_id
        JOIN tbl_pegawai ON tbl_layanan.lay_dokter_id = tbl_pegawai.pegawai_id
        WHERE tbl_pegawai.pegawai_id = '$d' AND tbl_layanan.layanan_tgl_masuk between '$t1' AND '$t2'");

        return $qry->result();
    }

    public function getDokter()
    {
        return $this->db->query("SELECT * FROM tbl_pegawai WHERE pegawai_jabatan LIKE 'Dokter%'")->result();
    }

    public function getDiagnosa()
    {
        return $this->db->get('tbl_diagnosa')->result();
    }

    public function getIndexDiagnosa($d, $t1, $t2)
    {
        $qdiag = $this->db->query("SELECT tbl_pasien.pasien_norm, tbl_pasien.pasien_nama, 
        year(curdate())-year(tbl_pasien.pasien_dlahir) AS umur,
        tbl_kamar.kamar_ruangan, tbl_kelas.kelas_nama, tbl_diagnosa.diagnosa_kode, tbl_diagnosa.diagnosa_nama,
        tbl_layanan.layanan_tgl_masuk, tbl_layanan.layanan_tgl_keluar,
        tbl_layanan.layanan_cara_keluar, tbl_layanan.layanan_sts_krs
        FROM tbl_layanan
        JOIN tbl_pasien ON tbl_layanan.lay_pasien_id = tbl_pasien.pasien_id
        JOIN tbl_kamar ON tbl_layanan.lay_kamar_id = tbl_kamar.kamar_id
        JOIN tbl_kelas ON tbl_layanan.lay_kelas_id = tbl_kelas.kelas_id
        JOIN tbl_diagnosa ON tbl_layanan.lay_diagnosa_id = tbl_diagnosa.diagnosa_id
        WHERE tbl_diagnosa.diagnosa_kode = '$d' AND 
        tbl_layanan.layanan_tgl_masuk between '$t1' AND '$t2'");

        return $qdiag->result();
    }

    public function getIndexDeath($d, $t1, $t2)
    {
        $qdeath = $this->db->query("SELECT tbl_pasien.pasien_norm, tbl_pasien.pasien_nama, 
        year(curdate())-year(tbl_pasien.pasien_dlahir) as umur,
        tbl_kamar.kamar_ruangan, tbl_kelas.kelas_nama, tbl_diagnosa.diagnosa_kode, 
        tbl_diagnosa.diagnosa_nama,
        tbl_layanan.layanan_tgl_masuk, tbl_layanan.layanan_tgl_keluar,
        tbl_layanan.layanan_cara_keluar, tbl_layanan.layanan_sts_krs
        from tbl_layanan
        join tbl_pasien on tbl_layanan.lay_pasien_id = tbl_pasien.pasien_id
        join tbl_kamar on tbl_layanan.lay_kamar_id = tbl_kamar.kamar_id
        join tbl_kelas on tbl_layanan.lay_kelas_id = tbl_kelas.kelas_id
        join tbl_diagnosa on tbl_layanan.lay_diagnosa_id = tbl_diagnosa.diagnosa_id
        where tbl_diagnosa.diagnosa_kode = '$d' and tbl_layanan.layanan_sts_krs = 'Meninggal' and
        tbl_layanan.layanan_tgl_masuk between '$t1' and '$t2'");

        return $qdeath->result();
    }

    public function getTindakan()
    {
        $qry = $this->db->query("SELECT * FROM tbl_tindakan WHERE tindakan_id NOT IN ('43')");
        return $qry->result();
    }

    public function getIndexTindakan($d, $t1, $t2)
    {
        $qtind = $this->db->query("SELECT tbl_pasien.pasien_norm, tbl_pasien.pasien_nama, 
        year(curdate())-year(tbl_pasien.pasien_dlahir) AS umur,
        tbl_kamar.kamar_ruangan, tbl_kelas.kelas_nama, tbl_tindakan.tindakan_kode, 
        tbl_tindakan.tindakan_nama,
        tbl_layanan.layanan_tgl_masuk, tbl_layanan.layanan_tgl_keluar,
        tbl_layanan.layanan_cara_keluar, tbl_layanan.layanan_sts_krs
        FROM tbl_layanan
        JOIN tbl_pasien ON tbl_layanan.lay_pasien_id = tbl_pasien.pasien_id
        JOIN tbl_kamar ON tbl_layanan.lay_kamar_id = tbl_kamar.kamar_id
        JOIN tbl_kelas ON tbl_layanan.lay_kelas_id = tbl_kelas.kelas_id
        JOIN tbl_diagnosa ON tbl_layanan.lay_diagnosa_id = tbl_diagnosa.diagnosa_id
        JOIN tbl_tindakan ON tbl_layanan.lay_tindakan_id = tbl_tindakan.tindakan_id
        WHERE tbl_tindakan.tindakan_kode = '$d' and
        tbl_layanan.layanan_tgl_masuk between '$t1' and '$t2'");

        return $qtind->result();
    }

    public function getDataGrafik()
    {
        $qgd = $this->db->query("SELECT tbl_layanan.lay_dokter_id, tbl_pegawai.pegawai_nama, 
        COUNT(tbl_layanan.lay_pasien_id) AS jumlah
        FROM tbl_layanan JOIN tbl_pegawai ON tbl_layanan.lay_dokter_id = tbl_pegawai.pegawai_id
        WHERE tbl_pegawai.pegawai_jabatan LIKE '%Dokter%'
        GROUP BY tbl_layanan.lay_dokter_id ");

        if($qgd->num_rows() > 0)
        {
            foreach($qgd->result() as $data)
            {
                $hasil[] = $data;
            }

            return $hasil;
        }
    }

    public function getTenIndex($u1, $u2)
    {
        $sqlten = $this->db->query("SELECT tbl_diagnosa.diagnosa_kode, tbl_diagnosa.diagnosa_nama,
                    year(curdate())-year(tbl_pasien.pasien_dlahir) AS umur
                    FROM tbl_layanan
                    JOIN tbl_pasien ON tbl_layanan.lay_pasien_id = tbl_pasien.pasien_id
                    JOIN tbl_kamar ON tbl_layanan.lay_kamar_id = tbl_kamar.kamar_id
                    JOIN tbl_kelas ON tbl_layanan.lay_kelas_id = tbl_kelas.kelas_id
                    JOIN tbl_diagnosa ON tbl_layanan.lay_diagnosa_id = tbl_diagnosa.diagnosa_id
                    WHERE year(curdate())-year(tbl_pasien.pasien_dlahir) between '$u1' AND '$u2'");
                    
        return $sqlten->result();
    }

    public function getIndexDiagnosaAll($t1, $t2)
    {
        $qdiagall = $this->db->query("SELECT tbl_pasien.pasien_norm, tbl_pasien.pasien_nama, 
        year(curdate())-year(tbl_pasien.pasien_dlahir) AS umur,
        tbl_kamar.kamar_ruangan, tbl_kelas.kelas_nama, tbl_diagnosa.diagnosa_kode, tbl_diagnosa.diagnosa_nama,
        tbl_layanan.layanan_tgl_masuk, tbl_layanan.layanan_tgl_keluar,
        tbl_layanan.layanan_cara_keluar, tbl_layanan.layanan_sts_krs
        FROM tbl_layanan
        JOIN tbl_pasien ON tbl_layanan.lay_pasien_id = tbl_pasien.pasien_id
        JOIN tbl_kamar ON tbl_layanan.lay_kamar_id = tbl_kamar.kamar_id
        JOIN tbl_kelas ON tbl_layanan.lay_kelas_id = tbl_kelas.kelas_id
        JOIN tbl_diagnosa ON tbl_layanan.lay_diagnosa_id = tbl_diagnosa.diagnosa_id
        WHERE tbl_layanan.layanan_tgl_masuk between '$t1' AND '$t2'");

        return $qdiagall->result();
    }

    public function getIndexMatiAll($t1, $t2)
    {
        $qdeathall = $this->db->query("SELECT tbl_pasien.pasien_norm, tbl_pasien.pasien_nama, 
        year(curdate())-year(tbl_pasien.pasien_dlahir) as umur,
        tbl_kamar.kamar_ruangan, tbl_kelas.kelas_nama, tbl_diagnosa.diagnosa_kode, 
        tbl_diagnosa.diagnosa_nama,
        tbl_layanan.layanan_tgl_masuk, tbl_layanan.layanan_tgl_keluar,
        tbl_layanan.layanan_cara_keluar, tbl_layanan.layanan_sts_krs
        FROM tbl_layanan
        JOIN tbl_pasien ON tbl_layanan.lay_pasien_id = tbl_pasien.pasien_id
        JOIN tbl_kamar ON tbl_layanan.lay_kamar_id = tbl_kamar.kamar_id
        JOIN tbl_kelas ON tbl_layanan.lay_kelas_id = tbl_kelas.kelas_id
        JOIN tbl_diagnosa ON tbl_layanan.lay_diagnosa_id = tbl_diagnosa.diagnosa_id
        WHERE tbl_layanan.layanan_sts_krs = 'Meninggal' AND
        tbl_layanan.layanan_tgl_masuk between '$t1' AND '$t2'");

        return $qdeathall->result();
    }

    public function getIndexTindAll($t1, $t2)
    {
        $qtindall = $this->db->query("SELECT tbl_pasien.pasien_norm, tbl_pasien.pasien_nama, 
        year(curdate())-year(tbl_pasien.pasien_dlahir) AS umur,
        tbl_kamar.kamar_ruangan, tbl_kelas.kelas_nama, tbl_tindakan.tindakan_kode, 
        tbl_tindakan.tindakan_nama,
        tbl_layanan.layanan_tgl_masuk, tbl_layanan.layanan_tgl_keluar,
        tbl_layanan.layanan_cara_keluar, tbl_layanan.layanan_sts_krs
        FROM tbl_layanan
        JOIN tbl_pasien ON tbl_layanan.lay_pasien_id = tbl_pasien.pasien_id
        JOIN tbl_kamar ON tbl_layanan.lay_kamar_id = tbl_kamar.kamar_id
        JOIN tbl_kelas ON tbl_layanan.lay_kelas_id = tbl_kelas.kelas_id
        JOIN tbl_diagnosa ON tbl_layanan.lay_diagnosa_id = tbl_diagnosa.diagnosa_id
        JOIN tbl_tindakan ON tbl_layanan.lay_tindakan_id = tbl_tindakan.tindakan_id
        WHERE tbl_layanan.layanan_tgl_masuk between '$t1' and '$t2'");

        return $qtindall->result();
    }

    public function getIndexDokterAll($t1, $t2)
    {
        $qrydokall = $this->db->query("SELECT tbl_layanan.layanan_id, tbl_pasien.pasien_norm, tbl_pasien.pasien_nama, tbl_pasien.pasien_dlahir, 
        tbl_unit.unit_nama, tbl_kelas.kelas_nama, tbl_layanan.layanan_tgl_masuk, 
        tbl_layanan.layanan_tgl_keluar, tbl_diagnosa.diagnosa_kode, tbl_diagnosa.diagnosa_nama,
        tbl_layanan.layanan_cara_keluar, tbl_pegawai.pegawai_nama
        FROM   tbl_layanan
        JOIN tbl_pasien ON tbl_layanan.lay_pasien_id = tbl_pasien.pasien_id
        JOIN tbl_unit ON tbl_layanan.lay_unit_id = tbl_unit.unit_id
        JOIN tbl_kelas ON tbl_layanan.lay_kelas_id = tbl_kelas.kelas_id
        JOIN tbl_diagnosa ON tbl_layanan.lay_diagnosa_id = tbl_diagnosa.diagnosa_id
        JOIN tbl_pegawai ON tbl_layanan.lay_dokter_id = tbl_pegawai.pegawai_id
        WHERE tbl_layanan.layanan_tgl_masuk between '$t1' AND '$t2'");

        return $qrydokall->result();
    }
}