<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kelurahan_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function getKelurahan()
    {
        // Query Join
        $this->db->select('*');
        $this->db->from('tbl_kelurahan');
        $this->db->join('tbl_kecamatan', 'tbl_kecamatan.kecamatan_id = tbl_kelurahan.kelurahan_kec_id');
        $qry = $this->db->get();
        return $qry->result();
    }

    public function getKecamatan()
    {
        $qry = $this->db->get('tbl_kecamatan');
        return $qry->result();
    }

    public function simpanKel($table, $data)
    {
        $res = $this->db->insert($table, $data);
        return $res;
    }

    public function viewby($id)
    {
        $qry = $this->db->query("
            SELECT tbl_kelurahan.kelurahan_id, tbl_kelurahan.kelurahan_nama, tbl_kelurahan.kelurahan_kec_id,
            tbl_kecamatan.kecamatan_id, tbl_kecamatan.kecamatan_nama
            FROM tbl_kelurahan, tbl_kecamatan
            WHERE tbl_kelurahan.kelurahan_kec_id = tbl_kecamatan.kecamatan_id AND
            tbl_kelurahan.kelurahan_id = '$id' 
        ");
        
        return $qry->row();
    }

    public function updateKel($id, $data)
    {
        $this->db->where('kelurahan_id', $id);
        return $this->db->update('tbl_kelurahan', $data);
    }

}