<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard_model extends CI_Model
{
    public function jumlahPRJ()
    {
        $qrj = $this->db->query("SELECT * FROM tbl_layanan WHERE lay_jenlan_id = '1' AND layanan_tgl_keluar IS NULL AND month(layanan_tgl_masuk)<='6'");
        $total = $qrj->num_rows();
        return $total;
    }

    public function jumlahPRI()
    {
        $qri = $this->db->query("SELECT * FROM tbl_layanan WHERE lay_jenlan_id = '2' AND layanan_tgl_keluar IS NULL AND month(layanan_tgl_masuk)<='6'");
        $total = $qri->num_rows();
        return $total;
    }

    public function jumlahKRS()
    {
        $qri = $this->db->query("SELECT * FROM tbl_layanan WHERE lay_jenlan_id = '2' AND layanan_tgl_keluar IS NOT NULL AND month(layanan_tgl_masuk)<='6'");
        $total = $qri->num_rows();
        return $total;
    }

    public function jumlahPM()
    {
        $qrip = $this->db->query("SELECT * FROM tbl_layanan WHERE lay_jenlan_id = '2' AND layanan_tgl_keluar IS NOT NULL AND layanan_sts_krs LIKE '%Meninggal%' AND month(layanan_tgl_masuk)<='6'");
        $total = $qrip->num_rows();
        return $total;
    }

    public function tenDiagnose()
    {
        $qtd = $this->db->query("SELECT tbl_layanan.lay_diagnosa_id, tbl_diagnosa.diagnosa_kode, tbl_diagnosa.diagnosa_nama, 
                                 COUNT(tbl_layanan.lay_diagnosa_id) AS jumlah
                                 FROM tbl_layanan JOIN tbl_diagnosa ON tbl_layanan.lay_diagnosa_id = tbl_diagnosa.diagnosa_id
                                 WHERE tbl_layanan.lay_diagnosa_id NOT IN ('15') AND month(tbl_layanan.layanan_tgl_keluar)<='6'
                                 GROUP BY tbl_layanan.lay_diagnosa_id ORDER BY jumlah DESC LIMIT 10");
        return $qtd->result();
    }

    public function tenTindakan()
    {
        $qtt = $this->db->query("SELECT tbl_layanan.lay_tindakan_id, tbl_tindakan.tindakan_kode, 
                                 tbl_tindakan.tindakan_nama, 
                                 COUNT(tbl_layanan.lay_tindakan_id) AS jumlah
                                 FROM tbl_layanan JOIN tbl_tindakan ON tbl_layanan.lay_tindakan_id = tbl_tindakan.tindakan_id
                                 WHERE tbl_layanan.lay_tindakan_id NOT IN ('43') AND month(tbl_layanan.layanan_tgl_keluar)<='6'
                                 GROUP BY tbl_layanan.lay_tindakan_id ORDER BY jumlah DESC LIMIT 10");
        return $qtt->result();
    }

    public function tenDeseaseDeath()
    {
        $qpk = $this->db->query("SELECT tbl_layanan.lay_diagnosa_id, tbl_diagnosa.diagnosa_kode, 
                                 tbl_diagnosa.diagnosa_nama, 
                                 COUNT(tbl_layanan.lay_diagnosa_id) as jumlah
                                 FROM tbl_layanan JOIN tbl_diagnosa ON tbl_layanan.lay_diagnosa_id = tbl_diagnosa.diagnosa_id
                                 WHERE tbl_layanan.lay_diagnosa_id NOT IN ('15') AND tbl_layanan.layanan_sts_krs = 'Meninggal' AND month(tbl_layanan.layanan_tgl_keluar)<='6'
                                 GROUP BY tbl_layanan.lay_diagnosa_id ORDER BY jumlah DESC LIMIT 10");
        return $qpk->result();
    }

    public function getDataGrafik()
    {
        $qgd = $this->db->query("SELECT tbl_layanan.lay_dokter_id, tbl_pegawai.pegawai_nama, 
        COUNT(tbl_layanan.lay_pasien_id) AS jumlah
        FROM tbl_layanan JOIN tbl_pegawai ON tbl_layanan.lay_dokter_id = tbl_pegawai.pegawai_id
        WHERE tbl_pegawai.pegawai_jabatan LIKE '%Dokter%' AND month(tbl_layanan.layanan_tgl_keluar)<='6'
        GROUP BY tbl_layanan.lay_dokter_id ");

        if($qgd->num_rows() > 0)
        {
            foreach($qgd->result() as $data)
            {
                $hasil[] = $data;
            }

            return $hasil;
        }
    }

    public function jumlahPRJ2()
    {
        $qrj = $this->db->query("SELECT * FROM tbl_layanan WHERE lay_jenlan_id = '1' AND layanan_tgl_keluar IS NULL AND month(layanan_tgl_masuk)>='7'");
        $total = $qrj->num_rows();
        return $total;
    }

    public function jumlahPRI2()
    {
        $qri = $this->db->query("SELECT * FROM tbl_layanan WHERE lay_jenlan_id = '2' AND layanan_tgl_keluar IS NULL AND month(layanan_tgl_masuk)>='7'");
        $total = $qri->num_rows();
        return $total;
    }

    public function jumlahKRS2()
    {
        $qri = $this->db->query("SELECT * FROM tbl_layanan WHERE lay_jenlan_id = '2' AND layanan_tgl_keluar IS NOT NULL AND month(layanan_tgl_masuk)>='7'");
        $total = $qri->num_rows();
        return $total;
    }

    public function jumlahPM2()
    {
        $qrip = $this->db->query("SELECT * FROM tbl_layanan WHERE lay_jenlan_id = '2' AND layanan_tgl_keluar IS NOT NULL AND layanan_sts_krs LIKE '%Meninggal%' AND month(layanan_tgl_masuk)>='7'");
        $total = $qrip->num_rows();
        return $total;
    }

    public function tenDiagnose2()
    {
        $qtd = $this->db->query("SELECT tbl_layanan.lay_diagnosa_id, tbl_diagnosa.diagnosa_kode, tbl_diagnosa.diagnosa_nama, 
                                 COUNT(tbl_layanan.lay_diagnosa_id) AS jumlah
                                 FROM tbl_layanan JOIN tbl_diagnosa ON tbl_layanan.lay_diagnosa_id = tbl_diagnosa.diagnosa_id
                                 WHERE tbl_layanan.lay_diagnosa_id NOT IN ('15') AND month(tbl_layanan.layanan_tgl_keluar)>='7'
                                 GROUP BY tbl_layanan.lay_diagnosa_id ORDER BY jumlah DESC LIMIT 10");
        return $qtd->result();
    }

    public function tenTindakan2()
    {
        $qtt = $this->db->query("SELECT tbl_layanan.lay_tindakan_id, tbl_tindakan.tindakan_kode, 
                                 tbl_tindakan.tindakan_nama, 
                                 COUNT(tbl_layanan.lay_tindakan_id) AS jumlah
                                 FROM tbl_layanan JOIN tbl_tindakan ON tbl_layanan.lay_tindakan_id = tbl_tindakan.tindakan_id
                                 WHERE tbl_layanan.lay_tindakan_id NOT IN ('43') AND month(tbl_layanan.layanan_tgl_keluar)>='7'
                                 GROUP BY tbl_layanan.lay_tindakan_id ORDER BY jumlah DESC LIMIT 10");
        return $qtt->result();
    }

    public function tenDeseaseDeath2()
    {
        $qpk = $this->db->query("SELECT tbl_layanan.lay_diagnosa_id, tbl_diagnosa.diagnosa_kode, 
                                 tbl_diagnosa.diagnosa_nama, 
                                 COUNT(tbl_layanan.lay_diagnosa_id) as jumlah
                                 FROM tbl_layanan JOIN tbl_diagnosa ON tbl_layanan.lay_diagnosa_id = tbl_diagnosa.diagnosa_id
                                 WHERE tbl_layanan.lay_diagnosa_id NOT IN ('15') AND tbl_layanan.layanan_sts_krs = 'Meninggal' AND month(tbl_layanan.layanan_tgl_keluar)>='7'
                                 GROUP BY tbl_layanan.lay_diagnosa_id ORDER BY jumlah DESC LIMIT 10");
        return $qpk->result();
    }

    public function getDataGrafik2()
    {
        $qgd = $this->db->query("SELECT tbl_layanan.lay_dokter_id, tbl_pegawai.pegawai_nama, 
        COUNT(tbl_layanan.lay_pasien_id) AS jumlah
        FROM tbl_layanan JOIN tbl_pegawai ON tbl_layanan.lay_dokter_id = tbl_pegawai.pegawai_id
        WHERE tbl_pegawai.pegawai_jabatan LIKE '%Dokter%' AND month(tbl_layanan.layanan_tgl_keluar)>='7'
        GROUP BY tbl_layanan.lay_dokter_id ");

        if($qgd->num_rows() > 0)
        {
            foreach($qgd->result() as $data)
            {
                $hasil[] = $data;
            }

            return $hasil;
        }
    }

}