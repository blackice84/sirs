<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pasien_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function getPasien()
    {
        $qry = $this->db->get('tbl_pasien');
        return $qry->result();
    }

    public function getPropinsi()
    {
        $prop = $this->db->get("tbl_propinsi");
        return $prop->result();
    }

    public function getKabupaten($id)
    {
        $kab = $this->db->query("SELECT tbl_kabupaten.kabupaten_id, tbl_kabupaten.kabupaten_nama
               FROM tbl_kabupaten, tbl_propinsi
               WHERE tbl_kabupaten.kabupaten_prop_id = tbl_propinsi.prop_id
               AND tbl_kabupaten.kabupaten_prop_id = '$id'");
        
        return $kab; 
    }

    public function getKecamatan($id)
    {
        $kec = $this->db->query("SELECT tbl_kecamatan.kecamatan_id, tbl_kecamatan.kecamatan_nama
               FROM tbl_kecamatan, tbl_kabupaten
               WHERE tbl_kecamatan.kecamatan_kab_id = tbl_kabupaten.kabupaten_id
               AND tbl_kecamatan.kecamatan_kab_id = '$id'");
        
        return $kec; 
    }

    public function simpanPasien($table, $data)
    {
        $qry = $this->db->insert($table, $data);
        return $qry;
    }
}

