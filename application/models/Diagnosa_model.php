<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Diagnosa_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function getDiagnosa()
    {
        $qry = $this->db->get('tbl_diagnosa');
        return $qry->result();
    }

    public function createDiag($table, $data)
    {
        $res = $this->db->insert($table, $data);
        return $res;
    }

    public function viewby($id)
    {
        $qry = $this->db->query("SELECT * FROM tbl_diagnosa WHERE diagnosa_id = '$id'");
        return $qry->row();
    }

    public function updateDiagnosa($id, $data)
    {
        $this->db->where('diagnosa_id', $id);
        return $this->db->update('tbl_diagnosa', $data);
    }
}