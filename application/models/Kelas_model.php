<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Kelas_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function getKelas()
    {
        $qry = $this->db->get('tbl_kelas');
        return $qry->result();
    }

    public function simpanKelas($table, $data)
    {
        $qry = $this->db->insert($table, $data);
        return $qry;
    }

    public function viewby($id)
    {
        $qry = $this->db->query("SELECT * FROM tbl_kelas WHERE kelas_id = '$id'");
        return $qry->row();
    }

    public function updateKelas($id, $data)
    {
        $this->db->where('kelas_id', $id);
        return $this->db->update('tbl_kelas', $data);
    }
}