<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Pegawai_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function createpeg($table, $data)
    {
        $res = $this->db->insert($table, $data);
        return $res;
    }

    public function getUnit()
    {
        $qry = $this->db->get('tbl_unit');
        return $qry->result();
    }

    public function getPegawai()
    {
        // Query Join
        $this->db->select('*');
        $this->db->from('tbl_pegawai');
        $this->db->join('tbl_unit', 'tbl_unit.unit_id = tbl_pegawai.pegawai_unit_id');
        $qry = $this->db->get();
        return $qry->result();
    }

    public function viewby($id)
    {
        $qry = $this->db->query("
            SELECT tbl_pegawai.pegawai_id, tbl_pegawai.pegawai_nama, tbl_pegawai.pegawai_alamat, tbl_pegawai.pegawai_telp, 
            tbl_pegawai.pegawai_pass, tbl_pegawai.pegawai_unit_id, tbl_unit.unit_nama
            FROM tbl_pegawai, tbl_unit
            WHERE tbl_pegawai.pegawai_unit_id = tbl_unit.unit_id AND
            tbl_pegawai.pegawai_id ='$id' 
        ");
        
        return $qry->row();
    }

    public function updatePegawai($id, $data)
    {
        $this->db->where('pegawai_id', $id);
        return $this->db->update('tbl_pegawai', $data);
    }
}