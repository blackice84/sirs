<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kamar_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    function getKamar()
    {
        $this->db->select('*');
        $this->db->from('tbl_kamar');
        $qry = $this->db->get();
        return $qry->result();
    }

    function getKelas()
    {
        $qry = $this->db->get('tbl_kelas');
        return $qry->result();
    }

    function simpanKamar($table, $data)
    {
        $qry = $this->db->insert($table, $data);
        return $qry;
    }

    function viewby($id)
    {
        $qry = $this->db->query(
            "SELECT tbl_kamar.kamar_id, tbl_kamar.kamar_ruangan, tbl_kamar.kamar_bed FROM tbl_kamar, tbl_kelas WHERE tbl_kamar.kamar_id = '$id'"
        );

        return $qry->row();
    }

    function updateKamar($id, $data)
    {
        $this->db->where('kamar_id', $id);
        return $this->db->update('tbl_kamar', $data);
    }
}