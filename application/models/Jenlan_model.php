<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Jenlan_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function getJenlan()
    {
        $qry = $this->db->get('tbl_jenislayanan');
        return $qry->result();
    }

    public function simpanJenlan($table, $data)
    {
        $qry = $this->db->insert($table, $data);
        return $qry;
    }

    public function viewby($id)
    {
        $qry = $this->db->query("SELECT * FROM tbl_jenislayanan WHERE jenlan_id = '$id'");
        return $qry->row();
    }

    public function updateJenlan($id, $data)
    {
        $this->db->where('jenlan_id', $id);
        return $this->db->update('tbl_jenislayanan', $data);
    }
}