<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class Registrasi_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    public function get_no_rm()
    {
        $qrm = $this->db->query("SELECT MAX(RIGHT(pasien_norm,6)) AS kd_max FROM tbl_pasien");
        $kd = "";

        if($qrm->num_rows() > 0)
        {
            foreach($qrm->result() as $k)
            {
                $tmp = ((int)$k->kd_max)+1;
                $kd = sprintf("%08s", $tmp);
            }
        } else {
            $kd = "00000001";
        }

        return $kd;
    }

    public function getJenisLayanan()
    {
        $qry = $this->db->get('tbl_jenislayanan');
        return $qry->result();
    }

    public function getTempatLayanan($id)
    {
        $hsl = $this->db->query("SELECT * FROM tbl_unit WHERE unit_jenlan_id = '$id'");
        return $hsl->result();
    }
    
    public function getKelas()
    {
        $qry = $this->db->get('tbl_kelas');
        return $qry->result();
    }

    public function getPasien($id)
    {
        $qry = $this->db->query("SELECT * FROM tbl_pasien WHERE pasien_norm = '$id'");
        return $qry->result_array();
    }

    // public function getDiagnosa()
    // {
    //     $qry = $this->db->get('tbl_diagnosa');
    //     return $qry->result();
    // }

    public function simpanDaftar($table, $data)
    {
        $qry = $this->db->insert($table, $data);
        return $qry;
    }

    public function lihatKunjungan()
    {
        $qrj = $this->db->query("SELECT tbl_pasien.pasien_norm, tbl_pasien.pasien_nama, tbl_layanan.layanan_tgl_masuk, 
                                tbl_jenislayanan.jenlan_nama, tbl_unit.unit_nama
                                FROM tbl_layanan JOIN tbl_pasien ON tbl_layanan.lay_pasien_id = tbl_pasien.pasien_id
                                JOIN tbl_jenislayanan ON tbl_layanan.lay_jenlan_id = tbl_jenislayanan.jenlan_id
                                JOIN tbl_unit ON tbl_layanan.lay_unit_id = tbl_unit.unit_id
            ");
        
        return $qrj->result();
    }
}