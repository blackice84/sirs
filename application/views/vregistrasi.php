<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>SIRS | Pendaftaran Pasien</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/adminlte/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/adminlte/bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/adminlte/bower_components/Ionicons/css/ionicons.min.css">
  <!-- bootstrap datepicker -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/adminlte/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
  <!-- Select2 -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/adminlte/bower_components/select2/dist/css/select2.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/adminlte/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/adminlte/dist/css/skins/_all-skins.min.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">

</head>
<body class="hold-transition skin-blue fixed sidebar-mini">
<!-- Site wrapper -->
<div class="wrapper">

  <header class="main-header">
    <!-- Logo -->
    <a href="<?php echo base_url(); ?>assets/adminlte/index2.html" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>A</b>LT</span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>SI</b>RS</span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </a>

      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <!-- Messages: style can be found in dropdown.less-->
          
          <!-- Notifications: style can be found in dropdown.less -->
          
          <!-- Tasks: style can be found in dropdown.less -->
         
          <!-- User Account: style can be found in dropdown.less -->
          <li class="dropdown user user-menu">
            <a href="<?php echo base_url(); ?>index.php/login/logout">  
              <span class="hidden-xs"><?php echo $this->session->userdata('ses_nama'); ?> (Logout)</span>
            </a>
          </li>
          <!-- Control Sidebar Toggle Button -->
          <li>
            <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
          </li>
        </ul>
      </div>
    </nav>
  </header>

  <!-- =============================================== -->

  <!-- Left side column. contains the sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
      <li class="header">MAIN NAVIGATION</li>
        <li class="treeview">
          <a href="#">
            <i class="fa fa-database"></i> <span>Master Data</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?php echo base_url(); ?>index.php/pasien"><i class="fa fa-user"></i> Data Pasien</a></li>
            <!-- <li><a href="<?php //echo base_url(); ?>index.php/pegawai"><i class="fa fa-user-md"></i> Data Pegawai</a></li>
            <li><a href="<?php //echo base_url(); ?>index.php/diagnosa"><i class="fa fa-stethoscope"></i> Data Diagnosa</a></li>
            <li><a href="<?php //echo base_url(); ?>index.php/unit"><i class="fa fa-building"></i> Data Unit</a></li>
            <li><a href="<?php //echo base_url(); ?>index.php/kamar"><i class="fa fa-bed"></i> Data Kamar</a></li>
            <li><a href="<?php //echo base_url(); ?>index.php/tindakan"><i class="fa fa-medkit"></i> Data Tindakan</a></li>
            <li><a href="<?php //echo base_url(); ?>index.php/kelas"><i class="fa fa-tasks"></i> Data Kelas</a></li>
            <li><a href="<?php //echo base_url(); ?>index.php/jenislay"><i class="fa fa-bookmark-o"></i> Data Jenis Layanan</a></li>
            <li><a href="<?php //echo base_url(); ?>index.php/propinsi"><i class="fa fa-map-marker"></i> Data Propinsi</a></li>
            <li><a href="<?php //echo base_url(); ?>index.php/kabupaten"><i class="fa fa-map-marker"></i> Data Kabupaten</a></li>
            <li><a href="<?php //echo base_url(); ?>index.php/kecamatan"><i class="fa fa-map-marker"></i> Data Kecamatan</a></li>
            <li><a href="<?php //echo base_url(); ?>index.php/kelurahan"><i class="fa fa-map-marker"></i> Data Kelurahan</a></li> -->
          </ul>
        </li>
        <!-- Transaksi -->
        <li class="treeview active">
          <a href="#">
            <i class="fa fa-exchange"></i> <span>Transaksi</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?php echo base_url(); ?>index.php/registrasi"><i class="fa fa-book"></i> Registrasi Pasien</a></li>
            <li><a href="<?php echo base_url(); ?>index.php/registrasi/kunjungan"><i class="fa fa-user-md"></i> Kunjungan Pasien</a></li>
            <!-- <li><a href="<?php echo base_url(); ?>index.php/pelayanan"><i class="fa fa-user-md"></i> Pelayanan Pasien</a></li> -->
          </ul>
        </li>
        <!-- Laporan -->
        <!-- <li class="treeview">
          <a href="#">
            <i class="fa fa-file-text-o"></i> <span>Laporan</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="#"><i class="fa fa-book"></i> Laporan 10 Besar Penyakit</a></li>
            <li><a href="#"><i class="fa fa-book"></i> Laporan Pasien Meninggal</a></li>
          </ul>
        </li> -->
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>

  <!-- =============================================== -->

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Pendaftaran
        <small>Pasien</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">SIMRS</a></li>
        <li class="active">Pendaftaran Pasien</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">Form Pendaftaran Pasien</h3>

          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                    title="Collapse">
              <i class="fa fa-minus"></i></button>
            <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fa fa-times"></i></button>
          </div>
        </div>
        <div class="box-body">
          <form action="<?php echo base_url(); ?>index.php/registrasi/simpanreg" method="POST">
            <div class="row">
              <div class="col-md-3">
                <!-- <label>No.RM</label> -->
                <div class="input-group input-group-sm">
                  <input type="hidden" id="pasien_id" name="pasien_id">
                  <input id="norm" type="text" name="norm" class="form-control" placeholder="No RM">
                </div>
              </div>
            
              <div class="col-md-9">
                <!-- <label>Nama Pasien</label> -->
                <div class="input-group input-group-sm">
                  <span class="input-group-addon"><i class="fa fa-user"></i></span>
                  <input id="nama" name="nama" type="text" class="form-control" readonly>
                </div>
              </div>  
            </div><br>
            <!-- end row -->
            <div class="row">
              <div class="col-md-3">
                  <label>NIK</label>
                <div class="input-group input-group-sm">
                  <div class="input-group-addon">
                    <i class="fa fa-credit-card"></i>
                  </div>
                  <input id="nik" name="nik" type="text" class="form-control" readonly>
                </div>
              </div>
              <!-- <div class="col-md-3">
                <label>Jenis Kelamin</label>
                <div class="form-group input-group-sm">
                  <input name="jenkel" type="text" class="form-control" readonly>
                </div>
              </div> -->
              <div class="col-md-6">
                <label>Tempat Lahir</label>
                <div class="form-group input-group-sm">
                  <input id="tmpt" type="text" class="form-control input-sm" name="tmp_lahir" readonly>
                </div>
              </div>
            </div>
            <!-- end row -->

            <div class="row">
              <div class="col-md-3">
                <div class="form-group">
                    <label>Tanggal Lahir</label>
                    <div class="input-group date input-group-sm">
                      <div class="input-group-addon">
                        <i class="fa fa-calendar"></i>
                      </div>
                      <input id="tgl" name="tgl_lahir" type="text" class="form-control pull-right" readonly>
                    </div>
                    <!-- /.input group -->
                </div>
                <!-- /.form group -->
              </div>

              <!-- <div class="col-md-5">
                <div class="form-group">
                <label>Diagnosa</label>
                <select class="form-control input-sm select2" style="width: 100%;" name="diagnosa">
                  <option selected="selected">- Pilih Diagnosa -</option>
                  <?php
                    //foreach($diagnosa as $diag)
                    //{
                  ?>
                    <option value="<?php //echo $diag->diagnosa_id; ?>"><?php //echo $diag->diagnosa_kode ?> - <?php //echo $diag->diagnosa_nama; ?></option>
                  <?php
                    //}
                  ?>
                </select>
                </div>
              </div> -->

              <!-- <div class="col-md-4">
                <div class="form-group">
                  <label>Agama</label>
                  <select name="agama" class="form-control input-sm">
                    <option selected>- Pilih Agama -</option>
                    <option value="Islam">Islam</option>
                    <option value="Kristen">Kristen</option>
                    <option value="Katolik">Katolik</option>
                    <option value="Hindu">Hindu</option>
                    <option value="Budha">Budha</option>
                  </select>
                </div>
              </div> -->

              <!-- <div class="col-md-5">
                  <div class="form-group">
                    <label>Propinsi</label>
                    <select name="propinsi" class="form-control input-sm">
                      <option selected>- Pilih Propinsi -</option>
                      <option value="Jawa Timur">Jawa Timur</option>
                      <option value="Jawa Tengah">Jawa Tengah</option>
                      <option value="Jawa Barat">Jawa Barat</option>
                    </select>
                  </div>
                </div> -->
 
            </div>
            <!-- end row -->

            <div class="row">
              <!-- <div class="col-md-4">
                <div class="form-group">
                  <label>Kabupaten/Kota</label>
                  <select name="kabupaten" class="form-control input-sm">
                    <option selected>- Pilih Kabupaten/Kota -</option>
                  </select>
                </div>
              </div> -->

              <!-- <div class="col-md-4">
                <div class="form-group">
                  <label>Kecamatan</label>
                  <select name="kecamatan" class="form-control input-sm">
                    <option selected>- Pilih Kecamatan -</option>
                  </select>
                </div>
              </div> -->

              <!-- <div class="col-md-4">
                <div class="form-group">
                  <label>Kelurahan</label>
                  <select name="kelurahan" class="form-control input-sm">
                    <option selected>- Pilih Kelurahan -</option>
                  </select>
                </div>
              </div> -->

            </div>
            <!-- end row -->

            <div class="row">

              <!-- <div class="col-md-8">
                <div class="form-group">
                  <label>Alamat</label>
                  <input type="text" name="alamat" class="form-control input-sm" placeholder="Alamat">
                </div>
              </div>

              <div class="col-md-2">
                <div class="form-group">
                  <label>RT</label>
                  <input type="text" name="rt" class="form-control input-sm" placeholder="RT">
                </div>
              </div>

              <div class="col-md-2">
                <div class="form-group">
                  <label>RW</label>
                  <input type="text" name="rw" class="form-control input-sm" placeholder="RW">
                </div>
              </div> -->

            </div> <!-- end row -->

            <div class="row">
              <!-- <div class="col-md-4">
                <div class="form-group">
                  <label>No. Telp</label>
                  <input type="text" name="telp" class="form-control input-sm" placeholder="No Telepon">
                </div>
              </div>

              <div class="col-md-4">
                <div class="form-group">
                  <label>Pekerjaan</label>
                  <select name="pekerjaan" class="form-control input-sm">
                    <option selected>- Pilih Pekerjaan -</option>
                  </select>
                </div>
              </div>

              <div class="col-md-4">
                <div class="form-group">
                  <label>Pendidikan</label>
                  <select name="pendidikan" class="form-control input-sm">
                    <option selected>- Pilih Pendidikan -</option>
                  </select>
                </div>
              </div> -->

            </div>

            <div class="row">

              <div class="col-md-4">
                <div class="form-group">
                  <label>Jenis Layanan</label>
                  <select name="jenlan" class="form-control input-sm" id="jenlan">
                    <option selected>- Pilih Jenis Layanan -</option>
                    <?php
                      foreach($jenlan as $jl)
                      {
                    ?>
                        <option value="<?php echo $jl->jenlan_id; ?>"><?php echo $jl->jenlan_nama; ?></option>
                    <?php
                      }
                    ?>
                  </select>
                </div>
              </div>

              <div class="col-md-4">
                <div class="form-group">
                  <label>Tempat Layanan</label>
                  <select name="tmplay" class="form-control input-sm" id="unit">
                    <option selected>- Pilih Tempat Layanan -</option>
                  </select>
                </div>
              </div>

              <div class="col-md-4">
                  <div class="form-group">
                    <label>Kelas</label>
                    <select name="kelas" class="form-control input-sm">
                      <option selected>- Pilih Kelas -</option>
                      <?php
                        foreach($kelas as $kelas)
                        {
                      ?>
                        <option value="<?php echo $kelas->kelas_id; ?>"><?php echo $kelas->kelas_nama; ?></option>
                      <?php
                        }
                      ?>
                    </select>
                  </div>
                </div>

            </div>


          
        </div>
        <!-- /.box-body -->
        <div class="box-footer">
          <button class="btn btn-primary" type="submit">SIMPAN</button>
          <button class="btn btn-danger" type="reset">BATAL</button>
        </div>
        </form>
        <!-- /.box-footer-->
      </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <!-- <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 2.4.0
    </div>
    <strong>Copyright &copy; 2014-2016 <a href="https://adminlte.io">Almsaeed Studio</a>.</strong> All rights
    reserved.
  </footer> -->

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Create the tabs -->
    <ul class="nav nav-tabs nav-justified control-sidebar-tabs">
      <li><a href="#control-sidebar-home-tab" data-toggle="tab"><i class="fa fa-home"></i></a></li>

      <li><a href="#control-sidebar-settings-tab" data-toggle="tab"><i class="fa fa-gears"></i></a></li>
    </ul>
    <!-- Tab panes -->
    <div class="tab-content">
      <!-- Home tab content -->
      <div class="tab-pane" id="control-sidebar-home-tab">
        <h3 class="control-sidebar-heading">Recent Activity</h3>
        <ul class="control-sidebar-menu">
          <li>
            <a href="javascript:void(0)">
              <i class="menu-icon fa fa-birthday-cake bg-red"></i>

              <div class="menu-info">
                <h4 class="control-sidebar-subheading">Langdon's Birthday</h4>

                <p>Will be 23 on April 24th</p>
              </div>
            </a>
          </li>
          <li>
            <a href="javascript:void(0)">
              <i class="menu-icon fa fa-user bg-yellow"></i>

              <div class="menu-info">
                <h4 class="control-sidebar-subheading">Frodo Updated His Profile</h4>

                <p>New phone +1(800)555-1234</p>
              </div>
            </a>
          </li>
          <li>
            <a href="javascript:void(0)">
              <i class="menu-icon fa fa-envelope-o bg-light-blue"></i>

              <div class="menu-info">
                <h4 class="control-sidebar-subheading">Nora Joined Mailing List</h4>

                <p>nora@example.com</p>
              </div>
            </a>
          </li>
          <li>
            <a href="javascript:void(0)">
              <i class="menu-icon fa fa-file-code-o bg-green"></i>

              <div class="menu-info">
                <h4 class="control-sidebar-subheading">Cron Job 254 Executed</h4>

                <p>Execution time 5 seconds</p>
              </div>
            </a>
          </li>
        </ul>
        <!-- /.control-sidebar-menu -->

        <h3 class="control-sidebar-heading">Tasks Progress</h3>
        <ul class="control-sidebar-menu">
          <li>
            <a href="javascript:void(0)">
              <h4 class="control-sidebar-subheading">
                Custom Template Design
                <span class="label label-danger pull-right">70%</span>
              </h4>

              <div class="progress progress-xxs">
                <div class="progress-bar progress-bar-danger" style="width: 70%"></div>
              </div>
            </a>
          </li>
          <li>
            <a href="javascript:void(0)">
              <h4 class="control-sidebar-subheading">
                Update Resume
                <span class="label label-success pull-right">95%</span>
              </h4>

              <div class="progress progress-xxs">
                <div class="progress-bar progress-bar-success" style="width: 95%"></div>
              </div>
            </a>
          </li>
          <li>
            <a href="javascript:void(0)">
              <h4 class="control-sidebar-subheading">
                Laravel Integration
                <span class="label label-warning pull-right">50%</span>
              </h4>

              <div class="progress progress-xxs">
                <div class="progress-bar progress-bar-warning" style="width: 50%"></div>
              </div>
            </a>
          </li>
          <li>
            <a href="javascript:void(0)">
              <h4 class="control-sidebar-subheading">
                Back End Framework
                <span class="label label-primary pull-right">68%</span>
              </h4>

              <div class="progress progress-xxs">
                <div class="progress-bar progress-bar-primary" style="width: 68%"></div>
              </div>
            </a>
          </li>
        </ul>
        <!-- /.control-sidebar-menu -->

      </div>
      <!-- /.tab-pane -->
      <!-- Stats tab content -->
      <div class="tab-pane" id="control-sidebar-stats-tab">Stats Tab Content</div>
      <!-- /.tab-pane -->
      <!-- Settings tab content -->
      <div class="tab-pane" id="control-sidebar-settings-tab">
        <form method="post">
          <h3 class="control-sidebar-heading">General Settings</h3>

          <div class="form-group">
            <label class="control-sidebar-subheading">
              Report panel usage
              <input type="checkbox" class="pull-right" checked>
            </label>

            <p>
              Some information about this general settings option
            </p>
          </div>
          <!-- /.form-group -->

          <div class="form-group">
            <label class="control-sidebar-subheading">
              Allow mail redirect
              <input type="checkbox" class="pull-right" checked>
            </label>

            <p>
              Other sets of options are available
            </p>
          </div>
          <!-- /.form-group -->

          <div class="form-group">
            <label class="control-sidebar-subheading">
              Expose author name in posts
              <input type="checkbox" class="pull-right" checked>
            </label>

            <p>
              Allow the user to show his name in blog posts
            </p>
          </div>
          <!-- /.form-group -->

          <h3 class="control-sidebar-heading">Chat Settings</h3>

          <div class="form-group">
            <label class="control-sidebar-subheading">
              Show me as online
              <input type="checkbox" class="pull-right" checked>
            </label>
          </div>
          <!-- /.form-group -->

          <div class="form-group">
            <label class="control-sidebar-subheading">
              Turn off notifications
              <input type="checkbox" class="pull-right">
            </label>
          </div>
          <!-- /.form-group -->

          <div class="form-group">
            <label class="control-sidebar-subheading">
              Delete chat history
              <a href="javascript:void(0)" class="text-red pull-right"><i class="fa fa-trash-o"></i></a>
            </label>
          </div>
          <!-- /.form-group -->
        </form>
      </div>
      <!-- /.tab-pane -->
    </div>
  </aside>
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->
<!-- jQuery 3 -->
<script src="<?php echo base_url(); ?>assets/adminlte/bower_components/jquery/dist/jquery.min.js"></script>

<script type="text/javascript">
  $(document).ready(function(){

    $('#norm').change(function(){
      var normfromfield = $('#norm').val();
      $.ajax({
        method: "POST",
        url: "<?php echo base_url(); ?>index.php/registrasi/getpasien",
        data: {norm: normfromfield},
        dataType: 'json',
        success: function(data){
          // awesome
          $.each(data, function(idx, obj){
            $('#pasien_id').val(obj.pasien_id);
            $('#nama').val(obj.pasien_nama);
            $('#nik').val(obj.pasien_nik);
            $('#tmpt').val(obj.pasien_tlahir);
            $('#tgl').val(obj.pasien_dlahir);
          });
        }
      })
    });

  }); 
</script>

<!-- Bootstrap 3.3.7 -->
<script src="<?php echo base_url(); ?>assets/adminlte/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- bootstrap datepicker -->
<script src="<?php echo base_url(); ?>assets/adminlte/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<!-- bootstrap color picker -->
<script src="<?php echo base_url(); ?>assets/adminlte/bower_components/bootstrap-colorpicker/dist/js/bootstrap-colorpicker.min.js"></script>
<!-- bootstrap time picker -->
<script src="<?php echo base_url(); ?>assets/adminlte/plugins/timepicker/bootstrap-timepicker.min.js"></script>
<!-- Select2 -->
<script src="<?php echo base_url(); ?>assets/adminlte/bower_components/select2/dist/js/select2.full.min.js"></script>
<!-- SlimScroll -->
<script src="<?php echo base_url(); ?>assets/adminlte/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="<?php echo base_url(); ?>assets/adminlte/bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="<?php echo base_url(); ?>assets/adminlte/dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?php echo base_url(); ?>assets/adminlte/dist/js/demo.js"></script>

<script>
  $(function () {
    //Initialize Select2 Elements
    $('.select2').select2()
  });
</script>

<script>
  $(document).ready(function () {
    $('.sidebar-menu').tree()

    //Date picker
    $('#datepicker').datepicker({
      autoclose: true,
      format: 'yyyy-mm-dd',
      todayHighlight: true
    })

  })
</script>

<!-- JQUERY AJAX -->
<script type="text/javascript">
  $(document).ready(function(){
    $('#jenlan').change(function(){
      var id = $(this).val();
        $.ajax({
          url: "<?php echo base_url(); ?>index.php/registrasi/getunit",
          method: "POST",
          data: {id:id},
          dataType: 'json',
          success: function(data){
            var html = '';
            var i;
            for(i=0; i<data.length; i++){
              html += '<option value='+data[i].unit_id+'>'+data[i].unit_nama+'</option>';
            }
            $('#unit').html(html);
          }
        })
    });

  });
</script>



</body>
</html>
