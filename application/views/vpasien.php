<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>SIRS | Pasien</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/adminlte/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/adminlte/bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/adminlte/bower_components/Ionicons/css/ionicons.min.css">
  <!-- daterange picker -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/adminlte/bower_components/bootstrap-daterangepicker/daterangepicker.css">
  <!-- bootstrap datepicker -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/adminlte/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
  <!-- iCheck for checkboxes and radio inputs -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/adminlte/plugins/iCheck/all.css">
  <!-- Bootstrap Color Picker -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/adminlte/bower_components/bootstrap-colorpicker/dist/css/bootstrap-colorpicker.min.css">
  <!-- Bootstrap time Picker -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/adminlte/plugins/timepicker/bootstrap-timepicker.min.css">
  <!-- Select2 -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/adminlte/bower_components/select2/dist/css/select2.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/adminlte/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/adminlte/dist/css/skins/_all-skins.min.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition skin-blue fixed sidebar-mini">
<!-- Site wrapper -->
<div class="wrapper">

  <header class="main-header">
    <!-- Logo -->
    <a href="<?php echo base_url(); ?>index.php/dashboard" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>A</b>LT</span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>SI</b>RS</span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </a>

      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <!-- Messages: style can be found in dropdown.less-->
          
          <!-- Notifications: style can be found in dropdown.less -->
          
          <!-- Tasks: style can be found in dropdown.less -->
         
          <!-- User Account: style can be found in dropdown.less -->
          <li class="dropdown user user-menu">
            <a href="<?php echo base_url(); ?>index.php/login/logout">  
              <span class="hidden-xs"><?php echo $this->session->userdata('ses_nama'); ?> (Logout)</span>
            </a>
          </li>
          <!-- Control Sidebar Toggle Button -->
          <li>
            <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
          </li>
        </ul>
      </div>
    </nav>
  </header>

  <!-- =============================================== -->

  <!-- Left side column. contains the sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
    
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">MAIN NAVIGATION</li>
        <li class="active treeview">
          <a href="#">
            <i class="fa fa-database"></i> <span>Master Data</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?php echo base_url(); ?>index.php/pasien"><i class="fa fa-user"></i> Data Pasien</a></li>
            <li><a href="<?php echo base_url(); ?>index.php/pegawai"><i class="fa fa-user-md"></i> Data Pegawai</a></li>
            <li><a href="<?php echo base_url(); ?>index.php/diagnosa"><i class="fa fa-stethoscope"></i> Data Diagnosa</a></li>
            <li><a href="<?php echo base_url(); ?>index.php/unit"><i class="fa fa-building"></i> Data Unit</a></li>
            <li><a href="<?php echo base_url(); ?>index.php/kamar"><i class="fa fa-bed"></i> Data Kamar</a></li>
            <li><a href="<?php echo base_url(); ?>index.php/tindakan"><i class="fa fa-medkit"></i> Data Tindakan</a></li>
            <li><a href="<?php echo base_url(); ?>index.php/kelas"><i class="fa fa-tasks"></i> Data Kelas</a></li>
            <li><a href="<?php echo base_url(); ?>index.php/jenislay"><i class="fa fa-bookmark-o"></i> Data Jenis Layanan</a></li>
            <li><a href="<?php echo base_url(); ?>index.php/propinsi"><i class="fa fa-map-marker"></i> Data Propinsi</a></li>
            <li><a href="<?php echo base_url(); ?>index.php/kabupaten"><i class="fa fa-map-marker"></i> Data Kabupaten</a></li>
            <li><a href="<?php echo base_url(); ?>index.php/kecamatan"><i class="fa fa-map-marker"></i> Data Kecamatan</a></li>
            <li><a href="<?php echo base_url(); ?>index.php/kelurahan"><i class="fa fa-map-marker"></i> Data Kelurahan</a></li>
          </ul>
        </li>
        <!-- Transaksi -->
        <li class="treeview">
          <a href="#">
            <i class="fa fa-exchange"></i> <span>Transaksi</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?php echo base_url(); ?>index.php/registrasi"><i class="fa fa-book"></i> Registrasi Pasien</a></li>
            <li><a href="<?php echo base_url(); ?>index.php/registrasi/kunjungan"><i class="fa fa-user-md"></i> Kunjungan Pasien</a></li>
            <li><a href="#"><i class="fa fa-user-md"></i> Pelayanan Pasien</a></li>
          </ul>
        </li>
        <!-- Laporan -->
        <li class="treeview">
          <a href="#">
            <i class="fa fa-file-text-o"></i> <span>Laporan</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="#"><i class="fa fa-book"></i> Laporan 10 Besar Penyakit</a></li>
            <li><a href="#"><i class="fa fa-book"></i> Laporan Pasien Meninggal</a></li>
          </ul>
        </li>
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>

  <!-- =============================================== -->

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Pasien
        <small>master data</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li><a href="#">Master Data</a></li>
        <li class="active">Pasien</li>
      </ol>
    </section>

    <!-- Main content -->
    <br><br>
    <section class="content">
    <form action="<?php echo base_url(); ?>index.php/pasien/simpanpasien" method="POST">
      <!-- Default box -->
      <div class="box-body">
          
            <div class="row">
              <div class="col-md-3">
                <!-- <label>No.RM</label> -->
                <div class="input-group input-group-sm">
                  <div class="input-group-btn">
                    <button id="btnrm" type="button" class="btn btn-danger">RM</button>
                  </div>
                  <input id="no_rm" type="text" name="no_rm" class="form-control" placeholder="no rekam medik">
                </div>
              </div>
            
              <div class="col-md-9">
                <!-- <label>Nama Pasien</label> -->
                <div class="input-group input-group-sm">
                  <span class="input-group-addon"><i class="fa fa-user"></i></span>
                  <input name="nama_pasien" type="text" class="form-control" placeholder="nama pasien">
                </div>
              </div>  
            </div><br>
            <!-- end row -->
            <div class="row">
              <div class="col-md-3">
                  <label>NIK</label>
                <div class="input-group input-group-sm">
                  <div class="input-group-addon">
                    <i class="fa fa-credit-card"></i>
                  </div>
                  <input name="nik" type="text" class="form-control" data-inputmask='"mask": "99.99.99.99.99.99.9999"' data-mask>
                </div>
              </div>
              <div class="col-md-3">
                <label>Jenis Kelamin</label>
                <div class="form-group">
                  <select name="jenkel" class="form-control input-sm">
                    <option value="L">Laki-Laki</option>
                    <option value="P">Perempuan</option>
                  </select>
                </div>
              </div>
              <div class="col-md-6">
                <label>Tempat Lahir</label>
                <div class="form-group">
                  <input type="text" class="form-control input-sm" name="tmp_lahir">
                </div>
              </div>
            </div>
            <!-- end row -->

            <div class="row">
              <div class="col-md-3">
                <div class="form-group">
                    <label>Tanggal Lahir</label>
                    <div class="input-group date input-group-sm">
                      <div class="input-group-addon">
                        <i class="fa fa-calendar"></i>
                      </div>
                      <input name="tgl_lahir" type="text" class="form-control pull-right" id="datepicker">
                    </div>
                    <!-- /.input group -->
                </div>
                <!-- /.form group -->
              </div>

              <div class="col-md-4">
                <div class="form-group">
                  <label>Agama</label>
                  <select name="agama" class="form-control input-sm">
                    <option selected>- Pilih Agama -</option>
                    <option value="Islam">Islam</option>
                    <option value="Kristen">Kristen</option>
                    <option value="Katolik">Katolik</option>
                    <option value="Hindu">Hindu</option>
                    <option value="Budha">Budha</option>
                  </select>
                </div>
              </div>

              <div class="col-md-5">
                  <div class="form-group">
                    <label>Propinsi</label>
                    <select name="id_prop" id="id_prop" class="form-control input-sm">
                      <?php
                        echo "<option>- Pilih Propinsi -</option>";
                        foreach($prop as $pp)
                        {
                      ?>
                          <option value="<?php echo $pp->prop_id; ?>"><?php echo $pp->prop_nama; ?></option>
                      <?php
                        }
                      ?>
                    </select>
                  </div>
                </div>
 
            </div>
            <!-- end row -->

            <div class="row">
              <div class="col-md-4">
                <div class="form-group">
                  <label>Kabupaten/Kota</label>
                  <div id="kab">
                  
                  </div>
                </div>
              </div>

              <div class="col-md-4">
                <div class="form-group">
                  <label>Kecamatan</label>
                  <div id="kec">

                  </div>
                </div>
              </div>

              <div class="col-md-4">
                <div class="form-group">
                  <label>Kelurahan</label>
                  <select name="kelurahan" class="form-control input-sm" id="kelurahan">
                    <option selected>- Pilih Kelurahan -</option>
                  <?php
                    foreach($kel as $kel){
                  ?>
                      <option class="<?php echo $kel->kelurahan_kec_id; ?>" value="<?php echo $kel->kelurahan_id; ?>"><?php echo $kel->kelurahan_nama; ?></option>
                  <?php
                    }
                  ?>
                  </select>
                </div>
              </div>

            </div>
            <!-- end row -->

            <div class="row">

              <div class="col-md-8">
                <div class="form-group">
                  <label>Alamat</label>
                  <input type="text" name="alamat" class="form-control input-sm" placeholder="Alamat">
                </div>
              </div>

              <div class="col-md-2">
                <div class="form-group">
                  <label>RT</label>
                  <input type="text" name="rt" class="form-control input-sm" placeholder="RT">
                </div>
              </div>

              <div class="col-md-2">
                <div class="form-group">
                  <label>RW</label>
                  <input type="text" name="rw" class="form-control input-sm" placeholder="RW">
                </div>
              </div>

            </div> <!-- end row -->

            <div class="row">
              <div class="col-md-4">
                <div class="form-group">
                  <label>No. Telp</label>
                  <input type="text" name="telp" class="form-control input-sm" placeholder="No Telepon">
                </div>
              </div>

              <div class="col-md-4">
                <div class="form-group">
                  <label>Pekerjaan</label>
                  <select name="pekerjaan" class="form-control input-sm">
                    <option selected>- Pilih Pekerjaan -</option>
                    <option value="Pegawai Negeri">Pegawai Negeri</option>
                    <option value="Pegawai Swasta">Pegawai Swasta</option>
                    <option value="Wirausaha">Wirausaha</option>
                    <option value="Polri">Polri</option>
                    <option value="TNI">TNI</option>
                    <option value="Mahasiswa">Mahasiswa</option>
                    <option value="Siswa">Siswa</option>
                    <option value="Guru">Guru</option>
                    <option value="Petani">Petani</option>
                  </select>
                </div>
              </div>

              <div class="col-md-4">
                <div class="form-group">
                  <label>Pendidikan</label>
                  <select name="pendidikan" class="form-control input-sm">
                    <option selected>- Pilih Pendidikan -</option>
                    <option value="SD">SD</option>
                    <option value="SLTP">SLTP</option>
                    <option value="SLTA">SLTA</option>
                    <option value="Diploma">Diploma</option>
                    <option value="Sarjana">Sarjana</option>
                  </select>
                </div>
              </div>

            </div>          
        </div>
        <!-- /.box-body -->
        <div class="box-footer">
          <button type="submit" class="btn btn-primary">Simpan</button>
          <a href="<?php echo base_url(); ?>index.php/pasien" class="btn btn-danger">Batal</a>
        </div>
      <!-- /.box -->
    </form>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 2.4.0
    </div>
    <strong>Copyright &copy; 2014-2016 <a href="https://adminlte.io">Almsaeed Studio</a>.</strong> All rights
    reserved.
  </footer>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Create the tabs -->
    <ul class="nav nav-tabs nav-justified control-sidebar-tabs">
      <li><a href="#control-sidebar-home-tab" data-toggle="tab"><i class="fa fa-home"></i></a></li>

      <li><a href="#control-sidebar-settings-tab" data-toggle="tab"><i class="fa fa-gears"></i></a></li>
    </ul>
    <!-- Tab panes -->
    <div class="tab-content">
      <!-- Home tab content -->
      <div class="tab-pane" id="control-sidebar-home-tab">
        <h3 class="control-sidebar-heading">Recent Activity</h3>
        <ul class="control-sidebar-menu">
          <li>
            <a href="javascript:void(0)">
              <i class="menu-icon fa fa-birthday-cake bg-red"></i>

              <div class="menu-info">
                <h4 class="control-sidebar-subheading">Langdon's Birthday</h4>

                <p>Will be 23 on April 24th</p>
              </div>
            </a>
          </li>
          <li>
            <a href="javascript:void(0)">
              <i class="menu-icon fa fa-user bg-yellow"></i>

              <div class="menu-info">
                <h4 class="control-sidebar-subheading">Frodo Updated His Profile</h4>

                <p>New phone +1(800)555-1234</p>
              </div>
            </a>
          </li>
          <li>
            <a href="javascript:void(0)">
              <i class="menu-icon fa fa-envelope-o bg-light-blue"></i>

              <div class="menu-info">
                <h4 class="control-sidebar-subheading">Nora Joined Mailing List</h4>

                <p>nora@example.com</p>
              </div>
            </a>
          </li>
          <li>
            <a href="javascript:void(0)">
              <i class="menu-icon fa fa-file-code-o bg-green"></i>

              <div class="menu-info">
                <h4 class="control-sidebar-subheading">Cron Job 254 Executed</h4>

                <p>Execution time 5 seconds</p>
              </div>
            </a>
          </li>
        </ul>
        <!-- /.control-sidebar-menu -->

        <h3 class="control-sidebar-heading">Tasks Progress</h3>
        <ul class="control-sidebar-menu">
          <li>
            <a href="javascript:void(0)">
              <h4 class="control-sidebar-subheading">
                Custom Template Design
                <span class="label label-danger pull-right">70%</span>
              </h4>

              <div class="progress progress-xxs">
                <div class="progress-bar progress-bar-danger" style="width: 70%"></div>
              </div>
            </a>
          </li>
          <li>
            <a href="javascript:void(0)">
              <h4 class="control-sidebar-subheading">
                Update Resume
                <span class="label label-success pull-right">95%</span>
              </h4>

              <div class="progress progress-xxs">
                <div class="progress-bar progress-bar-success" style="width: 95%"></div>
              </div>
            </a>
          </li>
          <li>
            <a href="javascript:void(0)">
              <h4 class="control-sidebar-subheading">
                Laravel Integration
                <span class="label label-warning pull-right">50%</span>
              </h4>

              <div class="progress progress-xxs">
                <div class="progress-bar progress-bar-warning" style="width: 50%"></div>
              </div>
            </a>
          </li>
          <li>
            <a href="javascript:void(0)">
              <h4 class="control-sidebar-subheading">
                Back End Framework
                <span class="label label-primary pull-right">68%</span>
              </h4>

              <div class="progress progress-xxs">
                <div class="progress-bar progress-bar-primary" style="width: 68%"></div>
              </div>
            </a>
          </li>
        </ul>
        <!-- /.control-sidebar-menu -->

      </div>
      <!-- /.tab-pane -->
      <!-- Stats tab content -->
      <div class="tab-pane" id="control-sidebar-stats-tab">Stats Tab Content</div>
      <!-- /.tab-pane -->
      <!-- Settings tab content -->
      <div class="tab-pane" id="control-sidebar-settings-tab">
        <form method="post">
          <h3 class="control-sidebar-heading">General Settings</h3>

          <div class="form-group">
            <label class="control-sidebar-subheading">
              Report panel usage
              <input type="checkbox" class="pull-right" checked>
            </label>

            <p>
              Some information about this general settings option
            </p>
          </div>
          <!-- /.form-group -->

          <div class="form-group">
            <label class="control-sidebar-subheading">
              Allow mail redirect
              <input type="checkbox" class="pull-right" checked>
            </label>

            <p>
              Other sets of options are available
            </p>
          </div>
          <!-- /.form-group -->

          <div class="form-group">
            <label class="control-sidebar-subheading">
              Expose author name in posts
              <input type="checkbox" class="pull-right" checked>
            </label>

            <p>
              Allow the user to show his name in blog posts
            </p>
          </div>
          <!-- /.form-group -->

          <h3 class="control-sidebar-heading">Chat Settings</h3>

          <div class="form-group">
            <label class="control-sidebar-subheading">
              Show me as online
              <input type="checkbox" class="pull-right" checked>
            </label>
          </div>
          <!-- /.form-group -->

          <div class="form-group">
            <label class="control-sidebar-subheading">
              Turn off notifications
              <input type="checkbox" class="pull-right">
            </label>
          </div>
          <!-- /.form-group -->

          <div class="form-group">
            <label class="control-sidebar-subheading">
              Delete chat history
              <a href="javascript:void(0)" class="text-red pull-right"><i class="fa fa-trash-o"></i></a>
            </label>
          </div>
          <!-- /.form-group -->
        </form>
      </div>
      <!-- /.tab-pane -->
    </div>
  </aside>
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="<?php echo base_url(); ?>assets/adminlte/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="<?php echo base_url(); ?>assets/adminlte/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- Select2 -->
<script src="<?php echo base_url(); ?>assets/adminlte/bower_components/select2/dist/js/select2.full.min.js"></script>
<!-- InputMask -->
<script src="<?php echo base_url(); ?>assets/adminlte/plugins/input-mask/jquery.inputmask.js"></script>
<script src="<?php echo base_url(); ?>assets/adminlte/plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
<script src="<?php echo base_url(); ?>assets/adminlte/plugins/input-mask/jquery.inputmask.extensions.js"></script>
<!-- date-range-picker -->
<script src="<?php echo base_url(); ?>assets/adminlte/bower_components/moment/min/moment.min.js"></script>
<script src="<?php echo base_url(); ?>assets/adminlte/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
<!-- bootstrap datepicker -->
<script src="<?php echo base_url(); ?>assets/adminlte/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>

<!-- SlimScroll -->
<script src="<?php echo base_url(); ?>assets/adminlte/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="<?php echo base_url(); ?>assets/adminlte/bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="<?php echo base_url(); ?>assets/adminlte/dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?php echo base_url(); ?>assets/adminlte/dist/js/demo.js"></script>


<script>
  $(document).ready(function () {
    $('.sidebar-menu').tree()

    //Initialize Select2 Elements
    $('.select2').select2()

    //Datemask dd/mm/yyyy
    $('#datemask').inputmask('dd/mm/yyyy', { 'placeholder': 'dd/mm/yyyy' })
    //Datemask2 mm/dd/yyyy
    $('#datemask2').inputmask('mm/dd/yyyy', { 'placeholder': 'mm/dd/yyyy' })
    //Money Euro
    $('[data-mask]').inputmask()

    //Date range picker
    $('#reservation').daterangepicker()
    //Date range picker with time picker
    $('#reservationtime').daterangepicker({ timePicker: true, timePickerIncrement: 30, format: 'MM/DD/YYYY h:mm A' })
    //Date range as a button
    $('#daterange-btn').daterangepicker(
      {
        ranges   : {
          'Today'       : [moment(), moment()],
          'Yesterday'   : [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
          'Last 7 Days' : [moment().subtract(6, 'days'), moment()],
          'Last 30 Days': [moment().subtract(29, 'days'), moment()],
          'This Month'  : [moment().startOf('month'), moment().endOf('month')],
          'Last Month'  : [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        },
        startDate: moment().subtract(29, 'days'),
        endDate  : moment()
      },
      function (start, end) {
        $('#daterange-btn span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'))
      }
    )

    //Date picker
    $('#datepicker').datepicker({
      autoclose: true,
      format: 'yyyy-mm-dd',
      todayHighlight: true
    })



  })
</script>

<script>
  $("#id_prop").change(function(){
    var id_prop = {id_prop : $("#id_prop").val()};
    $.ajax({
      type: "POST",
      url: "<?php echo base_url(); ?>index.php/pasien/kab",
      data: id_prop,
      success: function(msg)
      {
        $("#kab").html(msg);
      }
    });
  });
</script>

<script>
  $("#id_kab").change(function(){
    var id_kab = {id_kab : $("#id_kab").val()};
    $.ajax({
      type: "POST",
      url: "<?php echo base_url(); ?>index.php/pasien/kec",
      data: id_kab,
      success: function(msg)
      {
        $("#kec").html(msg);
      }
    });
  });
</script>

<script type="text/javascript">
  $(document).ready(function(){
    $('#btnrm').click(function(){
      $.getJSON("<?php echo base_url('index.php/registrasi/get_mr_code'); ?>", function(data){
        $('#no_rm').val(data);
      });
    });
  });
</script>
</body>
</html>
