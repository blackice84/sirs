<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>SIRS | Index Penyakit</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/adminlte/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/adminlte/bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/adminlte/bower_components/Ionicons/css/ionicons.min.css">
  <!-- bootstrap datepicker -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/adminlte/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/adminlte/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="<?php echo base_url(); ?>assets/adminlte/dist/css/skins/_all-skins.min.css">

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition skin-blue sidebar-mini">
<!-- Site wrapper -->
<div class="wrapper">

  <header class="main-header">
    <!-- Logo -->
    <a href="<?php echo base_url(); ?>index.php/dashboard" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>A</b>LT</span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>SI</b>RS</span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </a>

      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <!-- Messages: style can be found in dropdown.less-->
          
          <!-- Notifications: style can be found in dropdown.less -->
          
          <!-- Tasks: style can be found in dropdown.less -->
         
          <!-- User Account: style can be found in dropdown.less -->
          <li class="dropdown user user-menu">
            <a href="<?php echo base_url(); ?>index.php/login/logout">  
              <span class="hidden-xs"><?php echo $this->session->userdata('ses_nama'); ?> (Logout)</span>
            </a>
          </li>
          <!-- Control Sidebar Toggle Button -->
          <li>
            <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
          </li>
        </ul>
      </div>
    </nav>
  </header>

  <!-- =============================================== -->

  <!-- Left side column. contains the sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
    
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">MAIN NAVIGATION</li>
        <!-- <li class="treeview">
          <a href="#">
            <i class="fa fa-database"></i> <span>Master Data</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?php //echo base_url(); ?>index.php/pasien"><i class="fa fa-user"></i> Data Pasien</a></li>
            <li><a href="<?php //echo base_url(); ?>index.php/pegawai"><i class="fa fa-user-md"></i> Data Pegawai</a></li>
            <li><a href="<?php //echo base_url(); ?>index.php/diagnosa"><i class="fa fa-stethoscope"></i> Data Diagnosa</a></li>
            <li><a href="<?php //echo base_url(); ?>index.php/unit"><i class="fa fa-building"></i> Data Unit</a></li>
            <li><a href="<?php //echo base_url(); ?>index.php/kamar"><i class="fa fa-bed"></i> Data Kamar</a></li>
            <li><a href="<?php //echo base_url(); ?>index.php/tindakan"><i class="fa fa-medkit"></i> Data Tindakan</a></li>
            <li><a href="<?php //echo base_url(); ?>index.php/kelas"><i class="fa fa-tasks"></i> Data Kelas</a></li>
            <li><a href="<?php //echo base_url(); ?>index.php/jenislay"><i class="fa fa-bookmark-o"></i> Data Jenis Layanan</a></li>
            <li><a href="<?php //echo base_url(); ?>index.php/propinsi"><i class="fa fa-map-marker"></i> Data Propinsi</a></li>
            <li><a href="<?php //echo base_url(); ?>index.php/kabupaten"><i class="fa fa-map-marker"></i> Data Kabupaten</a></li>
            <li><a href="<?php //echo base_url(); ?>index.php/kecamatan"><i class="fa fa-map-marker"></i> Data Kecamatan</a></li>
            <li><a href="<?php //echo base_url(); ?>index.php/kelurahan"><i class="fa fa-map-marker"></i> Data Kelurahan</a></li>
          </ul>
        </li> -->
        <!-- Transaksi -->
        <!-- <li class="treeview">
          <a href="#">
            <i class="fa fa-exchange"></i> <span>Transaksi</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="<?php //echo base_url(); ?>index.php/registrasi"><i class="fa fa-book"></i> Registrasi Pasien</a></li>
            <li><a href="<?php //echo base_url(); ?>index.php/registrasi/kunjungan"><i class="fa fa-user-md"></i> Kunjungan Pasien</a></li>
            <li><a href="#"><i class="fa fa-user-md"></i> Pelayanan Pasien</a></li>
          </ul>
        </li> -->
        <!-- Laporan -->
        <li class="active treeview">
          <a href="#">
            <i class="fa fa-file-text-o"></i> <span>Laporan</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <!-- <li><a href="#"><i class="fa fa-book"></i> Laporan 10 Besar Penyakit</a></li> -->
            <li><a href="<?php echo base_url(); ?>index.php/laporan/lapindexdokter"><i class="fa fa-book"></i> Preview Index Dokter</a></li>
            <li><a href="<?php echo base_url(); ?>index.php/laporan/lapcetakindexdokter"><i class="fa fa-book"></i> Cetak Index Dokter</a></li>
            <li><a href="<?php echo base_url(); ?>index.php/laporan/lapindexpenyakit"><i class="fa fa-book"></i> Preview Index Penyakit</a></li>
            <li><a href="<?php echo base_url(); ?>index.php/laporan/lapcetakindexpenyakit"><i class="fa fa-book"></i> Cetak Index Penyakit</a></li>
            <li><a href="<?php echo base_url(); ?>index.php/laporan/lapindexmati"><i class="fa fa-book"></i> Preview Index Kematian</a></li>
            <li><a href="<?php echo base_url(); ?>index.php/laporan/lapcetakindexmati"><i class="fa fa-book"></i> Cetak Index Kematian</a></li>
            <li><a href="<?php echo base_url(); ?>index.php/laporan/lapindextindakan"><i class="fa fa-book"></i> Preview Index Tindakan</a></li>
            <li><a href="<?php echo base_url(); ?>index.php/laporan/lapcetakindextind"><i class="fa fa-book"></i> Cetak Index Tindakan</a></li>
            
          </ul>
        </li>
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>

  <!-- =============================================== -->

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        Index Penyakit
        <small>preview report data</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Dashboard</a></li>
        <li><a href="#">Laporan</a></li>
        <li class="active">Index Penyakit</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
          <!-- <h3 class="box-title">Data Index Dokter</h3> -->
          <form action="<?php echo base_url(); ?>index.php/laporan/getlappenyakit" method="post">
            <div class="col-md-4">
              <select name="penyakit" class="form-control input-sm">
                <option value="">- Pilih Penyakit -</option>
                <?php
                  foreach($dg as $p)
                  {
                ?>
                   <option value="<?php echo $p->diagnosa_kode; ?>"><?php echo $p->diagnosa_kode; ?> - <?php echo $p->diagnosa_nama; ?></option>   
                <?php
                  }
                ?>
              </select>
            </div>
            <div class="col-md-3">
              <div class="input-group date input-group-sm">
                <div class="input-group-addon">
                  <i class="fa fa-calendar"></i>
                </div>
                <input name="dp1" type="text" class="form-control pull-right" id="dp1" placeholder="dari tanggal..">
              </div>
            </div>
            <div class="col-md-3">
              <div class="input-group date input-group-sm">
                <div class="input-group-addon">
                  <i class="fa fa-calendar"></i>
                </div>
                <input name="dp2" type="text" class="form-control pull-right" id="dp2" placeholder="sampai tanggal..">
              </div>
            </div>
            
            <button id="cari" class="btn btn-success btn-sm" type="submit"><i class="fa fa-file-excel-o"></i> EXCEL</button>
            
          </form>
          
          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip"
                    title="Collapse">
              <i class="fa fa-minus"></i></button>
            <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fa fa-times"></i></button>
          </div>
        </div>
        <div class="box-body" id="hasil">
          
        </div>
        <!-- /.box-body -->
        <div class="box-footer">
          
        </div>
        <!-- /.box-footer-->
      </div>
      <!-- /.box -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <footer class="main-footer">
    <div class="pull-right hidden-xs">
      <b>Version</b> 1.0.0
    </div>
    <strong>Copyright &copy; 2014-2016 <a href="https://adminlte.io">Almsaeed Studio</a>.</strong> All rights
    reserved.
  </footer>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Create the tabs -->
    <ul class="nav nav-tabs nav-justified control-sidebar-tabs">
      <li><a href="#control-sidebar-home-tab" data-toggle="tab"><i class="fa fa-home"></i></a></li>

      <li><a href="#control-sidebar-settings-tab" data-toggle="tab"><i class="fa fa-gears"></i></a></li>
    </ul>
    <!-- Tab panes -->
    <div class="tab-content">
      <!-- Home tab content -->
      <div class="tab-pane" id="control-sidebar-home-tab">
        <h3 class="control-sidebar-heading">Recent Activity</h3>
        <ul class="control-sidebar-menu">
          <li>
            <a href="javascript:void(0)">
              <i class="menu-icon fa fa-birthday-cake bg-red"></i>

              <div class="menu-info">
                <h4 class="control-sidebar-subheading">Langdon's Birthday</h4>

                <p>Will be 23 on April 24th</p>
              </div>
            </a>
          </li>
          <li>
            <a href="javascript:void(0)">
              <i class="menu-icon fa fa-user bg-yellow"></i>

              <div class="menu-info">
                <h4 class="control-sidebar-subheading">Frodo Updated His Profile</h4>

                <p>New phone +1(800)555-1234</p>
              </div>
            </a>
          </li>
          <li>
            <a href="javascript:void(0)">
              <i class="menu-icon fa fa-envelope-o bg-light-blue"></i>

              <div class="menu-info">
                <h4 class="control-sidebar-subheading">Nora Joined Mailing List</h4>

                <p>nora@example.com</p>
              </div>
            </a>
          </li>
          <li>
            <a href="javascript:void(0)">
              <i class="menu-icon fa fa-file-code-o bg-green"></i>

              <div class="menu-info">
                <h4 class="control-sidebar-subheading">Cron Job 254 Executed</h4>

                <p>Execution time 5 seconds</p>
              </div>
            </a>
          </li>
        </ul>
        <!-- /.control-sidebar-menu -->

        <h3 class="control-sidebar-heading">Tasks Progress</h3>
        <ul class="control-sidebar-menu">
          <li>
            <a href="javascript:void(0)">
              <h4 class="control-sidebar-subheading">
                Custom Template Design
                <span class="label label-danger pull-right">70%</span>
              </h4>

              <div class="progress progress-xxs">
                <div class="progress-bar progress-bar-danger" style="width: 70%"></div>
              </div>
            </a>
          </li>
          <li>
            <a href="javascript:void(0)">
              <h4 class="control-sidebar-subheading">
                Update Resume
                <span class="label label-success pull-right">95%</span>
              </h4>

              <div class="progress progress-xxs">
                <div class="progress-bar progress-bar-success" style="width: 95%"></div>
              </div>
            </a>
          </li>
          <li>
            <a href="javascript:void(0)">
              <h4 class="control-sidebar-subheading">
                Laravel Integration
                <span class="label label-warning pull-right">50%</span>
              </h4>

              <div class="progress progress-xxs">
                <div class="progress-bar progress-bar-warning" style="width: 50%"></div>
              </div>
            </a>
          </li>
          <li>
            <a href="javascript:void(0)">
              <h4 class="control-sidebar-subheading">
                Back End Framework
                <span class="label label-primary pull-right">68%</span>
              </h4>

              <div class="progress progress-xxs">
                <div class="progress-bar progress-bar-primary" style="width: 68%"></div>
              </div>
            </a>
          </li>
        </ul>
        <!-- /.control-sidebar-menu -->

      </div>
      <!-- /.tab-pane -->
      <!-- Stats tab content -->
      <div class="tab-pane" id="control-sidebar-stats-tab">Stats Tab Content</div>
      <!-- /.tab-pane -->
      <!-- Settings tab content -->
      <div class="tab-pane" id="control-sidebar-settings-tab">
        <form method="post">
          <h3 class="control-sidebar-heading">General Settings</h3>

          <div class="form-group">
            <label class="control-sidebar-subheading">
              Report panel usage
              <input type="checkbox" class="pull-right" checked>
            </label>

            <p>
              Some information about this general settings option
            </p>
          </div>
          <!-- /.form-group -->

          <div class="form-group">
            <label class="control-sidebar-subheading">
              Allow mail redirect
              <input type="checkbox" class="pull-right" checked>
            </label>

            <p>
              Other sets of options are available
            </p>
          </div>
          <!-- /.form-group -->

          <div class="form-group">
            <label class="control-sidebar-subheading">
              Expose author name in posts
              <input type="checkbox" class="pull-right" checked>
            </label>

            <p>
              Allow the user to show his name in blog posts
            </p>
          </div>
          <!-- /.form-group -->

          <h3 class="control-sidebar-heading">Chat Settings</h3>

          <div class="form-group">
            <label class="control-sidebar-subheading">
              Show me as online
              <input type="checkbox" class="pull-right" checked>
            </label>
          </div>
          <!-- /.form-group -->

          <div class="form-group">
            <label class="control-sidebar-subheading">
              Turn off notifications
              <input type="checkbox" class="pull-right">
            </label>
          </div>
          <!-- /.form-group -->

          <div class="form-group">
            <label class="control-sidebar-subheading">
              Delete chat history
              <a href="javascript:void(0)" class="text-red pull-right"><i class="fa fa-trash-o"></i></a>
            </label>
          </div>
          <!-- /.form-group -->
        </form>
      </div>
      <!-- /.tab-pane -->
    </div>
  </aside>
  <!-- /.control-sidebar -->
  <!-- Add the sidebar's background. This div must be placed
       immediately after the control sidebar -->
  <div class="control-sidebar-bg"></div>
</div>
<!-- ./wrapper -->

<!-- jQuery 3 -->
<script src="<?php echo base_url(); ?>assets/adminlte/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="<?php echo base_url(); ?>assets/adminlte/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- SlimScroll -->
<script src="<?php echo base_url(); ?>assets/adminlte/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- bootstrap datepicker -->
<script src="<?php echo base_url(); ?>assets/adminlte/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
<!-- FastClick -->
<script src="<?php echo base_url(); ?>assets/adminlte/bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="<?php echo base_url(); ?>assets/adminlte/dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?php echo base_url(); ?>assets/adminlte/dist/js/demo.js"></script>
<script>
  $(document).ready(function () {
    $('.sidebar-menu').tree()

    //Date picker
    $('#dp1').datepicker({
      autoclose: true,
      format: 'yyyy-mm-dd',
      todayHighlight: true
    })

    $('#dp2').datepicker({
      autoclose: true,
      format: 'yyyy-mm-dd',
      todayHighlight: true
    })

    $('#dp3').datepicker({
      autoclose: true,
      format: 'yyyy-mm-dd',
      todayHighlight: true
    })

    $('#dp4').datepicker({
      autoclose: true,
      format: 'yyyy-mm-dd',
      todayHighlight: true
    })

  })
</script>
<script>
  // $(document).ready(function(){
  //   $('#cari').click(function(){
  //     var dokter = $('#dokter').val();
  //     var dp1 = $('#dp1').val();
  //     var dp2 = $('#dp2').val();

  //     //alert(t1);

  //     if(t1 != '' && t2 != '')
  //     {
  //       $.ajax({
  //         url: "<?php //echo base_url(); ?>index.php/laporan/getindexdokter",
  //         method: 'POST',
  //         data: {dokter:dokter, dp1:dp1, dp2:dp2},
  //         dataType:'json',
  //         success: function(data)
  //         {
  //           alert(data);
  //         }  
  //       });
  //     } else {
  //       alert("Lengkapi input data");
  //     }
  //   });
  // });
</script>
</body>
</html>
