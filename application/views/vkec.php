<select name="id_kec" id="id_kec" class="form-control input-sm">
    <?php
        if(count($kec->result_array())>0)
        {
    ?>
        <option>- Pilih Kecamatan -</option>
    <?php
        foreach($kec->result_array() as $kc)
        {
    ?>
            <option value="<?php echo $kc['kecamatan_id']; ?>"><?php echo $kc['kecamatan_nama']; ?></option>
    <?php
        }
    ?>

    <?php
        } else {
            echo "<option>- Data belum tersedia -</option>";
        }
    ?>
</select>