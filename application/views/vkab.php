<select name="id_kab" id="id_kab" class="form-control input-sm">
    <?php
        if(count($kab->result_array())>0)
        {
    ?>
        <option>- Pilih Kabupaten -</option>
    <?php
        foreach($kab->result_array() as $kb)
        {
    ?>
            <option value="<?php echo $kb['kabupaten_id']; ?>"><?php echo $kb['kabupaten_nama']; ?></option>
    <?php
        }
    ?>

    <?php
        } else {
            echo "<option>- Data belum tersedia -</option>";
        }
    ?>
</select>